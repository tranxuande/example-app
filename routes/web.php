<?php

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\OrdersController;
use Illuminate\Support\Facades\Route;

// Home
Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

// Login
Route::get('login', [App\Http\Controllers\LoginController::class, 'index']);
Route::post('login', [App\Http\Controllers\LoginController::class, 'login']);
Route::get('logout', [App\Http\Controllers\LoginController::class, 'logout']);
// Admin
Route::group(['namespace' => 'Admin'], function () {
    Route::get('admin', [HomeController::class, 'index']);
    // Product
    Route::resource('product', '\App\Http\Controllers\Admin\ProductController', [
        'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
    ]);
    // Slide_image
    Route::resource('slide', '\App\Http\Controllers\Admin\SlideImageController', [
        'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
    ]);
    // Oder
    Route::resource('orders', '\App\Http\Controllers\Admin\OrdersController', [
        'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
    ]);
    Route::post('update-status', [OrdersController::class, 'update_status']);
    // News
    Route::resource('news', '\App\Http\Controllers\Admin\NewsController', [
        'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
    ]);
});


// Product view
Route::get('/product-show/{url}', [App\Http\Controllers\HomeController::class, 'product_detail']);

// search category
Route::get('/product-category/{url}', [App\Http\Controllers\HomeController::class, 'product_category']);

//search all
Route::get('/product-search', [App\Http\Controllers\HomeController::class, 'product_search']);

// display cart
Route::get('/cart-display', [App\Http\Controllers\HomeController::class, 'display_cart']);

// pay
Route::get('pay', [App\Http\Controllers\HomeController::class, 'pay']);
// payment methods
Route::get('payment-methods', [App\Http\Controllers\HomeController::class, 'payment_methods']);
// store pay
Route::post('pay', [App\Http\Controllers\HomeController::class, 'store_pay']);

//introduce
Route::get('/introduce', [App\Http\Controllers\HomeController::class, 'introduce']);

//news
Route::get('/page-news', [App\Http\Controllers\HomeController::class, 'news']);

// details news
Route::get('/details-news/{url}', [App\Http\Controllers\HomeController::class, 'details_news']);

// contact
Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact']);



// return policy
Route::get('/return-policy', [App\Http\Controllers\HomeController::class, 'return_policy']);

Route::get('vn-pay', [App\Http\Controllers\HomeController::class, 'vn_pay']);
Route::post('vnpay-create-payment', [App\Http\Controllers\HomeController::class, 'vnpay_create_payment']);
Route::get('vnpay-return', [App\Http\Controllers\HomeController::class, 'vnpay_return']);

