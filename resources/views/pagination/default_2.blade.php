
@if ($paginator->lastPage() > 1)
    <nav>

        <ul class="pagination">

            <li class="page-item {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }} ">
                <a class="page-link" href="{{ $paginator->appends(request()->input())->url(1) }}"><i class="fas fa-angle-double-left" style="color: #3490dc;"></i></a>
            </li>

            <li class="page-item {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }} ">
                <a class="page-link" href="{{ $paginator->appends(request()->input())->url($paginator->currentPage() - 1) }}"><i class="fas fa-angle-left" style="color: #3490dc;"></i></a>
            </li>

            @if ($paginator->currentPage() > 5)
                <li class="page-item disabled ">
                    <a class="page-link" >...</a>
                </li>
            @endif

            @for ($i = 1; $i <= $paginator->lastPage(); $i++)

                    @if(($i - 5) < $paginator->currentPage() && $paginator->currentPage() < ($i + 5))
                        <li class="page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                            @if (($paginator->currentPage() == $i))
                                <span class="page-link">
                                <span>{{$i}}</span>
                                <span class="sr-only">(current)</span>
                            </span>
                            @else
                                <a class="page-link" href="{{ $paginator->appends(request()->input())->url($i) }}">{{ $i }}</a>
                            @endif

                        </li>
                    @endif

            @endfor
            @if ($paginator->currentPage() < $paginator->lastPage() - 5)
                <li class="page-item disabled ">
                    <a class="page-link" >...</a>
                </li>
            @endif

            <li class="page-item {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                <a class="page-link" href="{{ $paginator->appends(request()->input())->url($paginator->currentPage()+1) }}"><i class="fas fa-angle-right" style="color: #3490dc;"></i></a>
            </li>

            <li class="page-item {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }} ">
                <a class="page-link" href="{{ $paginator->appends(request()->input())->url($paginator->lastPage()) }}"><i class="fas fa-angle-double-right" style="color: #3490dc;"></i></a>
            </li>
        </ul>
    </nav>
@endif
