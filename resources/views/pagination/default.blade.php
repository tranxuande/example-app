@if ($paginator->lastPage() > 1)
    <nav>
        <ul class="pagination">
            {{--Trang đầu tiên--}}
            <li class="page-item{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                @if($paginator->currentPage() == 1)
                    <span class="page-link">&laquo;</span>
                @else
                    <a class="page-link"
                       href="{{ $paginator->appends(request()->query())->url(1) }}">&laquo;</a>
                @endif
            </li>
            {{--Trang trước--}}
            <li class="page-item{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                @if($paginator->currentPage() == 1)
                    <span class="page-link">Trước</span>
                @else
                    <a class="page-link"
                       href="{{ $paginator->appends(request()->query())->url($paginator->currentPage() - 1) }}">Trước</a>
                @endif
            </li>
            {{--Trang trước ...--}}
            @if(($paginator->currentPage() - 5) > 0)
                <li class="page-item disabled">
                    <span class="page-link">...</span>
                </li>
            @endif
            {{--Link trang--}}
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                @if(($i - 5) < $paginator->currentPage() && $paginator->currentPage() < ($i + 5))
                    <li class="page-item{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                        @if($paginator->currentPage() == $i)
                            <span class="page-link">
                                <span>{{ $i }}</span>
                                <span class="sr-only">(current)</span>
                            </span>
                        @else
                            <a class="page-link"
                               href="{{ $paginator->appends(request()->query())->url($i) }}">{{ $i }}</a>
                        @endif
                    </li>
                @endif
            @endfor
            {{--Trang sau ...--}}
            @if(($paginator->currentPage() + 5) < $paginator->lastPage())
                <li class="page-item disabled">
                    <span class="page-link">...</span>
                </li>
            @endif
            {{--Trang sau--}}
            <li class="page-item{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                @if($paginator->currentPage() == $paginator->lastPage())
                    <span class="page-link">Sau</span>
                @else
                    <a class="page-link"
                       href="{{ $paginator->appends(request()->query())->url($paginator->currentPage() + 1) }}">Sau</a>
                @endif
            </li>
            {{--Trang cuối--}}
            <li class="page-item{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                @if($paginator->currentPage() == $paginator->lastPage())
                    <span class="page-link">&raquo;</span>
                @else
                    <a class="page-link"
                       href="{{ $paginator->appends(request()->query())->url($paginator->lastPage()) }}">&raquo;</a>
                @endif
            </li>
        </ul>
    </nav>
@endif
