@extends('layouts_admin.master')
@section('content')
    <div id="creat-product" class="container-fluid mt-3">
        <h4>THÔNG TIN CHUNG</h4>
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('message') !!}</p>
            @endif
        </div>
        <form class="mb-5" method="POST"
              action="{{ isset($product) ? '/product/' . $product->id  : route('product.store') }}"
              enctype="multipart/form-data">
            @csrf
            @if (isset($product))
                {{ method_field('PUT') }}
                <input type="hidden" name="id" value="{{$product->id}}">
            @endif
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">
                            Tên sản phẩm
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="name" id="name" type="text"
                                   value="{{ old('name', isset($product->name) ? $product->name : '') }}"
                                   class="form-control" required maxlength="250">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="category" class="col-sm-4 col-form-label">Phân loại<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="category_id" id="category" class="form-control" required>
                                <option value="">Chọn danh mục</option>
                                @foreach($category as $key => $value)
                                    <option
                                        value="{{$key}}" {{old('category_id') == $key || (isset($product) && $product->category_id == $key) ? 'selected' : ''}}>{{$value}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-4 col-form-label">Giá<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="number" id="price" name="price"
                                   value="{{ old('price',isset($product) ? $product->price : '') }}"
                                   class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="promotional_price" class="col-sm-4 col-form-label">Giá khuyến mại</label>
                        <div class="col-sm-8">
                            <input type="number" id="promotional_price" name="promotional_price" class="form-control"
                                   value="{{ old('promotional_price', isset($product) ? $product->promotional_price : '')  }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="quantily" class="col-sm-4 col-form-label">Số lượng</label>
                        <div class="col-sm-8">
                            <input type="number" id="quantily" name="quantily" class="form-control"
                                   value="{{ old('quantily', isset($product) ? $product->quantily : '')  }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Công khai</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="publish" value="0">
                            <input id="publish" name="publish" type="checkbox"
                                   value="1" {{ !!old('publish') || (isset($product->publish) && !!$product->publish) ? 'checked="checked"' : '' }}>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-form-label">Ảnh sản phẩm</label>
                        <div class="col-sm-12">
                            <div class="row">
                                @if(isset($product_image))
                                    @foreach ($product_image as $key => $value)
                                        <div class="col-md-3 mb-2 img_product1">
                                            <img src="{{asset($value->link)}}" alt="">
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <input class="mb-2" type="file" name="photos[]" multiple>
                            <input class="mb-2" type="file" name="photos[]" multiple>
                            <input class="mb-2" type="file" name="photos[]" multiple>
                            <input class="mb-2" type="file" name="photos[]" multiple>
                            <input class="mb-2" type="file" name="photos[]" multiple>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group row">
                        <label for="description" class="col-sm-4 col-form-label">Mô tả<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <textarea id="description" name="description" class="form-control"
                                      rows="3" required
                                      maxlength="500">{{ old('description', isset($product) ? $product->description : '')}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="content" class="col-sm-12 col-form-label">Chi tiết<span class="text-danger">*</span></label>
                        <div class="col-sm-12">
                            <textarea id="content" name="content" class="form-control"
                                      rows="3">{{ old('content', isset($product) ? $product->content : '')}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary w-25">Xác nhận</button>
            </div>
        </form>
    </div>
@stop
@section('scripts')
    <script>
        tinymce.init({
            selector: 'textarea#content',
            height: 300,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>
@stop
