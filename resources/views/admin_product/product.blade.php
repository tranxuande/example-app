@extends('layouts_admin.master')
@section('content')
    <div id="product" class="container-fluid">
        <div class="title mb-2">
            <h4>Danh sách sản phẩm</h4>
            <div class="btn-create-products">
                <a href="/product/create" class="btn btn-primary">
                    Tạo sản phẩm mới
                </a>
            </div>
        </div>
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('message') !!}</p>
            @endif
        </div>
        <div>
            <form class="row mb-2" action="">
                <div class="col-md-2">
                    <label for="name">Tên sản phẩm</label>
                    <input type="text" name="name" id="name" value="{{isset($_GET['name']) ? $_GET['name'] : ''}}"
                           class="form-control form-control-sm">
                </div>
                <div class="col-md-2">
                    <label for="category">Danh mục</label>
                    <select name="category"  id="category" class="form-control">
                        <option value="">Tất cả</option>
                        @foreach($category as $key => $value)
                            <option
                                value="{{$key}}" {{isset($_GET['category']) && $_GET['category'] == $key ? 'selected' : ''}}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="publish">Công khai</label>
                    <select name="publish" id="publish" class="form-control">
                        <option value="">Tất cả</option>
                        <option value="1" {{isset($_GET['publish']) && $_GET['publish'] == 1 ? 'selected' : ''}}>Có</option>
                        <option value="0" {{isset($_GET['publish']) && $_GET['publish'] == 0 ? 'selected' : ''}}>Không</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="price_from">Giá từ</label>
                    <input type="number" class="form-control form-control-sm" id="price_from" name="price_from"
                           value="{{isset($_GET['price_from']) ? $_GET['price_from'] : ''}}">
                </div>
                <div class="col-md-2">
                    <label for="price_to">Giá đến</label>
                    <input type="number" class="form-control form-control-sm" name="price_to" id="price_to"
                           value="{{isset($_GET['price_to']) ? $_GET['price_to'] : ''}}">
                </div>
                <div class="col-md-2">
                    <label for="date_from">Ngày tạo từ</label>
                    <input type="date" class="form-control form-control-sm" name="date_from" id="date_from"
                           value="{{isset($_GET['date_from']) ? $_GET['date_from'] : ''}}">
                </div>
                <div class="col-md-2">
                    <label for="date_to">Ngày tạo đến</label>
                    <input type="date" class="form-control form-control-sm" name="date_to" id="date_to"
                           value="{{isset($_GET['date_to']) ? $_GET['date_to'] : ''}}">
                </div>
                <div class="col-md-2">
                    <label class="w-100" for="" style="opacity: 0">Tìm kiếm</label>
                    <button type="submit" class="btn btn-primary w-100">Tìm kiếm</button>
                </div>
            </form>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3">
                <h4>
                    Tổng: {{$total_products->total_products}} sản phẩm
                </h4>
            </div>
            <div class="col-sm-3">
                <h4>
                    Khuyến mãi: {{$products_promotion->products_promotion}} sản phẩm
                </h4>
            </div>
            <div class="col-sm-4">
                <h4>
                    Chưa sử dụng: {{$products_not_displayed->products_not_displayed}} sản phẩm
                </h4>
            </div>
        </div>
        <div class="table-responsive-sm">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Hình ảnh</th>
                    <th>Tên sản phẩm</th>
                    <th>Danh mục</th>
                    <th>Giá</th>
                    <th>Giá khuyến mại</th>
                    <th>% giảm giá</th>
                    <th>Số lượng</th>
                    <th>Đã bán</th>
                    <th>Trạng thái</th>
                    <th>Ngày tạo</th>
                    <th>Ngày cập nhập</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($products as $key => $value)
                    <tr>
                        <td>{{($key + 1)}}</td>
                        <td class="img_product">
                            @if(isset($product_image[$value->id]))
                                <img src="{{isset($product_image[$value->id]) ? $product_image[$value->id] : '' }}" alt="">
                            @endif
                        </td>
                        <td>{{$value->name}}</td>
                        <td>{{$category[$value->category_id]}}</td>
                        <td>{{number_format($value->price)}}</td>
                        <td>{{number_format($value->promotional_price)}}</td>
                        <td>{{$value->percent_discount ? $value->percent_discount . '%' : ''}}</td>
                        <td class="text-center">{{$value->quantily}}</td>
                        <td class="text-center">{{$value->sold}}</td>
                        <td class="text-center">{{$value->publish ? '✓' : '' }}</td>
                        <td>{{date('d-m-Y H:i:s', strtotime($value->created_at))}}</td>
                        <td>{{date('d-m-Y H:i:s', strtotime($value->update_at))}}</td>
                        <td style="white-space: nowrap">
                            <a class="btn btn-sm btn-primary" href="/product/{{$value->id}}/edit"><i class="far fa-edit"></i> Sửa</a>
                        </td>
                        <td style="white-space: nowrap">
                            <form action="{{ url('/product', ['id' => $value->id]) }}" method="post" onsubmit="return confirm('Bạn chắc chắn muốn xóa sản phẩm này không?');">
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-sm btn-danger"><i class="far fa-trash-alt"></i> Xóa</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div>
            @include('pagination.default_2', ['paginator' => $products])
        </div>
    </div>
@stop
