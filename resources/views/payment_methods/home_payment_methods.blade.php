@extends('layouts.master')
@section('content')
    <div id="payment_methods" class="container text-center">
        <div class="text-center">
            <div>
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }} text-center">{!! Session::get('message') !!}</p>
                @endif
            </div>
            <h2 class="mt-5">Gửu hóa đơn thành công</h2>
            <h5>Chúng tôi sẽ liên hệ với bạn trong vòng 24h kể từ lúc nhận được đơn hàng</h5>
            <h5 class="">
                Sau khi xác nhận mua hàng bạn hãy liên hệ với cửa hàng qua:
                <br>
                <b >Số điện thoại: 0967894659</b >
                <br>
                <b>Gmail: daoquanghuythanhhai@gmail.com</b>
            </h5>
            <h5>Cảm ơn bạn đã ghé thăm fashion store và mua hàng</h5>
            <a href="/" class="btn btn-outline-primary w-50">Tiếp tục mua hàng</a>
        </div>
    </div>

@stop
