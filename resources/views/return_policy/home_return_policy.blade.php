@extends('layouts.master')
@section('content')
   <div class="container mt-5 mb-5" id="return_policy">
       <div>
           <h1>Quy định đổi trả hàng</h1>
           <h3>I. QUY ĐỊNH ĐỔI HÀNG ONLINE</h3>
           <h3 >1. CHÍNH SÁCH ÁP DỤNG</h3>
           <div class="accordion-content" style="font-size: 18px; padding-left: 30px;">
               <p>Áp dụng từ ngày 1/01/2022</p>
               <p>Trong vòng 30 ngày kể từ ngày mua sản phẩm với các sản phẩm của FASHION STORE</p>
               <ul>
                   <li>Áp dụng đối với sản phẩm nguyên giá và sản phẩm giảm giá</li>
                   <li>Sản phẩm nguyên giá chỉ được đổi 01 lần duy nhất sang sản phẩm nguyên giá khác và không thấp hơn giá trị sản phẩm đã mua. KH sẽ thanh toán phần tiền chênh lệch nếu sản phẩm đổi có giá trị cao hơn sản phẩm đã mua </li>
                   <li>Sản phẩm giảm giá/khuyến mại được đổi 01 lần sang màu khác hoặc size khác trên cùng 1 mã trong điều kiện còn sản phẩm hoặc theo quy chế chương trình (nếu có).</li>
                   <li>Chính sách chỉ áp dụng khi sản phẩm còn hóa đơn mua hàng, còn nguyên nhãn mác, thẻ bài đính kèm sản phẩm và sản phẩm không bị dơ bẩn, hư hỏng bởi những tác nhân bên ngoài cửa hàng sau khi mua sản phẩm.</li>
                   <li>Sản phẩm đồ lót và phụ kiện (trừ khăn len và mũ len) không được đổi.</li>

               </ul>
           </div>
           <div class="accordion-item">
               <h3 class="accordion-title">2. ĐIỀU KiỆN ĐỔI SẢN PHẨM</h3>
               <div class="accordion-content" style="font-size: 18px; padding-left: 30px;">
                   <ul>
                       <li>Đổi hàng trong vòng 30 ngày kể từ ngày khách hàng nhận được sản phẩm</li>
                       <li>Sản phẩm còn nguyên tem, mác và chưa qua sử dụng.</li>
                   </ul>
               </div>
           </div>
           <div class="accordion-item">
               <h3 class="accordion-title">3. THỰC HIỆN ĐỔI SẢN PHẨM</h3>
               <div class="accordion-content"  style="font-size: 18px; padding-left: 30px;">
                   <p>Từ ngày 1/01/2022, bạn có thể đổi hàng Online tại cửa hàng fashion store. Lưu ý: vui lòng mang theo sản phẩm và phiếu giao hàng.</p>
                   <p>Nếu tại khu vực bạn không có cửa hàng  FASHION STORE hoặc sản phẩm bạn muốn đổi thì vui lòng làm theo các bước sau:</p>
                   <p>Bước 1: Gọi đến Tổng đài 1800.6061 - nhánh 1 (0đ), cung cấp mã đơn hàng và mã sản phẩm cần đổi</p>
                   <p>Bước 2: Vui lòng gửi hàng đổi về địa chỉ : Kho Online - Công ty cổ phần  FASHION STORE - Km 379 - Ấp Kim Ngưu, xã Tân Tiến, huyện Văn Giang, tỉnh Hưng Yên.</p>
                   <p>Bước 3:  FASHION STORE gửi đổi sản phẩm mới khi nhận được hàng. Trong trường hợp hết hàng,  FASHION STORE sẽ liên hệ xác nhận</p>
                   <p>Lưu ý</p>
                   <ul>
                       <li>Đơn hàng không thuộc địa chỉ trên, Khách hàng vui lòng gửi hàng về Kho Online theo hướng dẫn</li>
                       <li>Thời gian nhận hàng: Sáng 08h30-12h, Chiều 13h-17h từ thứ 2 – thứ 6</li>
                       <li>Kho online không nhận giữ hàng trong thời gian khách hàng gửi sản phẩm về đổi hàng</li>
                   </ul>
               </div>
           </div>
       </div>
       <div class="accordion-item">
           <h3 class="accordion-title">II. QUY ĐỊNH TRẢ HÀNG ONLINE</h3>
           <div class="accordion-content">
               <div class="accordion-item">
                   <h3 class="accordion-title">1. CHÍNH SÁCH ÁP DỤNG</h3>
                   <div class="accordion-content" style="font-size: 18px; padding-left: 30px;">
                       <ul>
                           <li> FASHION STORE nhận lại sản phẩm trong trường hợp lỗi phát sinh từ nhà sản xuất</li>
                           <li>Các trường hợp lỗi do nhà sản xuất gồm: <span style="color: red;">ố màu, phai màu, lỗi chất liệu, lỗi đường may, lỗi kiểu dáng… không theo đúng mô tả và tiêu chuẩn sản phẩm</span></li>
                           <li>Hoàn tiền lại sản phẩm gặp lỗi qua tài khoản ngân hàng.</li>
                           <li> FASHION STORE miễn phí 100% chi phí trả hàng.</li>
                           <li> FASHION STORE sẽ xử lý trong vòng 10 ngày kể từ ngày nhận được sản phẩm lỗi</li>
                       </ul>
                   </div>
               </div>
               <div class="accordion-item">
                   <h3 class="accordion-title">2. ĐiỀU KIỆN TRẢ SẢN PHẨM</h3>
                   <div class="accordion-content" style="font-size: 18px; padding-left: 30px;">
                       <ul>
                           <li>Trả sản phẩm trong vòng 30 ngày kể từ ngày bạn nhận sản phẩm.</li>
                           <li>Sản phẩm còn nguyên tem, mác và chưa qua sử dụng.</li>
                       </ul>
                   </div>
               </div>

               <div class="accordion-item">
                   <h3 class="accordion-title">3. THỰC HIỆN TRẢ SẢN PHẨM</h3>
                   <div class="accordion-content" style="font-size: 18px; padding-left: 30px;">
                       <p>Bước 1: Gửi thông tin mã đơn hàng và tình trạng gặp lỗi vào địa chỉ mail saleonline@ FASHION STORE.com</p>
                       <p>Bước 2: Gửi sản phẩm lỗi về địa chỉ: <b>Kho Online - Công ty cổ phần  FASHION STORE - Km 379 - Ấp Kim Ngưu, xã Tân Tiến, huyện Văn Giang, tỉnh Hưng Yên.</b></p>
                   </div>
               </div>
           </div>
       </div>
       <div class="accordion-item active">
           <h3 class="accordion-title active">III. QUY ĐỊNH ĐỔI SẢN PHẨM MUA TẠI CỬA HÀNG</h3>
           <div class="accordion-content" style="font-size: 18px; padding-left: 30px;">
               <p>Áp dụng từ ngày 1/01/2022</p>
               <p>Trong vòng 30 ngày kể từ ngày mua sản phẩm với các sản phẩm của FASHION STORE.</p>
               <ul>
                   <li>Áp dụng đối với sản phẩm nguyên giá và sản phẩm giảm giá</li>
                   <li>Sản phẩm nguyên giá chỉ được đổi 01 lần duy nhất sang sản phẩm nguyên giá khác và không thấp hơn giá trị sản phẩm đã mua. . KH sẽ thanh toán phần tiền chênh lệch nếu sản phẩm đổi có giá trị cao hơn sản phẩm đã mua. </li>
                   <li>Sản phẩm giảm giá/khuyến mại được đổi 01 lần sang màu khác hoặc size khác trên cùng 1 mã trong điều kiện còn sản phẩm hoặc theo quy chế chương trình (nếu có).</li>
                   <li>Chính sách chỉ áp dụng khi sản phẩm còn hóa đơn mua hàng, còn nguyên nhãn mác, thẻ bài đính kèm sản phẩm và sản phẩm không bị dơ bẩn, hư hỏng bởi những tác nhân bên ngoài cửa hàng sau khi mua sản phẩm.</li>
                   <li>Sản phẩm đồ lót và phụ kiện (trừ khăn len và mũ len) không được đổi.</li>
                   <li>Hóa đơn xuất trình là hóa đơn gốc mua tại cửa hàng hoặc hóa đơn điện tử lấy từ ứng dụng FASHION STORE trên điện thoại.<p></p>
                   </li><li>Sản phẩm mua tại cửa hàng được đổi tại cửa hàng khác trên toàn hệ thống.</li>
                   <li>Mọi thắc mắc về quy định chung tại cửa hàng vui lòng liên hệ hotline: 1800.6061 (nhánh số 2) hoặc email: chamsockhachhang@fashionstore.com<p></p>
                   </li></ul>
           </div>
       </div>
   </div>

@stop
