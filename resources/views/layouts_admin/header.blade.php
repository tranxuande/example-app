<header class="mb-3">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">
            <img src="{{asset('/images/logo_fashion.jpg')}}" style="width: 50%" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="nav navbar-nav ">
                <li class="nav-item ">
                    <a class="nav-link {{ request()->is('admin*') ? 'active' : '' }}" href="/admin" >Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('product*') ? 'active' : '' }}" href="/product">Product</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('slide*') ? 'active' : '' }}" href="/slide">Slide Image</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('orders*') ? 'active' : '' }}" href="/orders">Orders</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('news*') ? 'active' : '' }}" href="/news">News</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
