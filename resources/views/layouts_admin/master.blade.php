<!DOCTYPE html>
<html lang="vi">
<head>
    <title>Admin</title>
    @include('layouts_admin.head_tag')
    @yield('plugins')
</head>
<body>
@include('layouts_admin.header')
@yield('content')
@yield('scripts')
</body>
</html>
