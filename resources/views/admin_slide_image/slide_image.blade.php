@extends('layouts_admin.master')
@section('content')
    <div id="home-slide" class="container-fluid">
        <div class="title mb-2">
            <h4>Danh sách slide ảnh</h4>
            <div class="btn-create-slide">
                <a href="/slide/create" class="btn btn-primary">
                    Tạo sản phẩm mới
                </a>
            </div>
        </div>
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('message') !!}</p>
            @endif
        </div>
        <div>
            <form class="row mb-2" action="">
                <div class="col-md-2">
                    <label for="">Tên slide</label>
                    <input type="text" name="name" value="{{isset($_GET['name']) ? $_GET['name'] : ''}}"
                           class="form-control form-control-sm">
                </div>
                <div class="col-md-2">
                    <label for="">Danh mục sản phẩm</label>
                    <select name="category" class="form-control">
                        <option value="">Tất cả</option>
                        @foreach($products as $key => $value)
                            <option
                                value="{{$key}}" {{isset($_GET['category']) && $_GET['category'] == $key ? 'selected' : ''}}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="">Công khai</label>
                    <select name="publish" class="form-control">
                        <option value="">Tất cả</option>
                        <option value="1" {{isset($_GET['publish']) && $_GET['publish'] == 1 ? 'selected' : ''}}>Có</option>
                        <option value="0" {{isset($_GET['publish']) && $_GET['publish'] == 0 ? 'selected' : ''}}>Không</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="">Ngày tạo từ</label>
                    <input type="date" class="form-control form-control-sm" name="date_from"
                           value="{{isset($_GET['date_from']) ? $_GET['date_from'] : ''}}">
                </div>
                <div class="col-md-2">
                    <label for="">Ngày tạo đến</label>
                    <input type="date" class="form-control form-control-sm" name="date_to"
                           value="{{isset($_GET['date_to']) ? $_GET['date_to'] : ''}}">
                </div>
                <div class="col-md-2">
                    <label class="w-100" for="" style="opacity: 0">Tìm kiếm</label>
                    <button type="submit" class="btn btn-primary w-100">Tìm kiếm</button>
                </div>
            </form>
        </div>
        <div class="row  mt-3">
            <div class="col-sm-3">
                <h4>
                    Tổng: {{$total_slide->total_slide}} quảng cáo
                </h4>
            </div>
            <div class="col-sm-3">
                <h4>
                    Chưa sử dụng: {{$slide_not_displayed->slide_not_displayed}} quảng cáo
                </h4>
            </div>
        </div>
        <div class="table-responsive-sm">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Hình ảnh slide</th>
                    <th>Tên slide</th>
                    <th>Danh mục sản phẩm</th>
                    <th>Trạng thái</th>
                    <th>Ngày tạo</th>
                    <th>Ngày cập nhập</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($slide_image as $key => $value)
                    <tr>
                        <td>{{($key + 1)}}</td>
                        <td class="img_product">
                            <img src="{{isset($value->link_image) ? $value->link_image : ''}}" alt="">
                        </td>
                        <td>{{$value->name}}</td>
                        <td>{{$products[$value->product_id]}}</td>
                        <td class="text-center">{{$value->publish ? '✓' : ''}}</td>
                        <td>{{date('d-m-Y H:i:s', strtotime($value->created_at))}}</td>
                        <td>{{date('d-m-Y H:i:s', strtotime($value->update_at))}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="/slide/{{$value->id}}/edit"><i
                                    class="far fa-edit"></i> Sửa</a>
                        </td>
                        <td>
                            <form action="{{ url('/slide', ['id' => $value->id]) }}" method="post"
                                  onsubmit="return confirm('Bạn chắc chắn muốn xóa sản phẩm này không?');">
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-sm btn-danger"><i class="far fa-trash-alt"></i> Xóa
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div>
            @include('pagination.default_2', ['paginator' => $slide_image])
        </div>
    </div>

@stop
