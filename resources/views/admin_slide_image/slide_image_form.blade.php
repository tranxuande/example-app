@extends('layouts_admin.master')
@section('content')
    <div id="creat-slide" class="container-fluid mt-3">
        <h4>THÔNG TIN CHUNG</h4>
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('message') !!}</p>
            @endif
        </div>
        <form action="{{isset($slide_image) ? '/slide/' . $slide_image->id : route('slide.store')}}" method="POST"  enctype="multipart/form-data">
            @csrf
            @if (isset($slide_image))
                {{ method_field('PUT') }}
                <input type="hidden" name="id" value="{{$slide_image->id}}">
            @endif
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">
                            Tên slide
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="name" id="name" type="text"
                                   value="{{ old('name', isset($slide_image->name) ? $slide_image->name : '') }}"
                                   class="form-control" required maxlength="250">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="product" class="col-sm-4 col-form-label">
                            Sản phẩm
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <select name="product_id" id="product" class="form-control" required>
                                <option value="">Chọn sản phẩm</option>
                                @foreach($products as $key => $value)
                                    <option
                                        value="{{$key}}" {{old('products') == $key || (isset($slide_image) && $slide_image->product_id == $key) ? 'selected' : ''}}>{{$value}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Công khai</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="publish" value="0">
                            <input id="publish" name="publish" type="checkbox"
                                   value="1" {{ !!old('publish') || (isset($slide_image->publish) && !!$slide_image->publish) ? 'checked="checked"' : '' }}>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">
                            Ảnh slide
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <div class="row">
                                @if(isset($slide_image->link_image))
                                        <div class="col-md-3 mb-2 img_product1">
                                            <img src="{{asset($slide_image->link_image)}}" alt="">
                                        </div>
                                @endif
                            </div>
                            <input class="mb-2" type="file" name="photos" multiple>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary w-25">Xác nhận</button>
            </div>
        </form>
    </div>
@stop
