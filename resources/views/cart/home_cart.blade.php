@extends('layouts.master')
@section('content')
    <div class="container mt-5 mb-5" id="cart">
        <h3>GIỎ HÀNG</h3>
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }} text-center">{!! Session::get('message') !!}</p>
            @endif
        </div>
        <div class="row">
            <table id="products_views" class="table">
                <thead>
                    <tr>
                        <th>Tên sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Giá tiền</th>
                        <th>Tổng tiền</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-md-6">
                <div class="form-group row">
                    <label for="note" class="col-sm-12 ">Ghi chú </label>
                    <div class="col-sm-12">
                    <textarea id="note" class="form-control" name="note" rows="3" ></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-lg-right ">
                <div style="padding-top: 75px">
                    <a  class="btn btn-primary  mr-3" href="/" >Tiếp tục mua hàng</a>
                    <a  class="pay btn btn-primary w-25" href="pay" >Thanh toán</a>
                </div>
            </div>
        </div>
    </div>
@stop
