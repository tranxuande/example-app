@extends('layouts.master')
@section('content')
    <div class="container text-center">
        <div class="header clearfix">
            <h3 class="text-muted mt-5">VNPAY RESPONSE</h3>
        </div>
        <div class="table-responsive">
            <div class="form-group">
                <label>Mã đơn hàng:</label>
                <label><?php echo $_GET['vnp_TxnRef'] ?></label>
            </div>
            <div class="form-group">
                <label>Số tiền:</label>
                <label><?php echo $_GET['vnp_Amount'] ?></label>
            </div>
            <div class="form-group">
                <label>Nội dung thanh toán:</label>
                <label><?php echo $_GET['vnp_OrderInfo'] ?></label>
            </div>
            <div class="form-group">
                <label>Mã phản hồi giao dịch:</label>
                <label><?php echo $_GET['vnp_ResponseCode'] ?></label>
            </div>
            <div class="form-group">
                <label>Mã GD Tại VNPAY:</label>
                <label><?php echo $_GET['vnp_TransactionNo'] ?></label>
            </div>
            <div class="form-group">
                <label>Ngân hàng GD:</label>
                <label><?php echo $_GET['vnp_BankCode'] ?></label>
            </div>
            <div class="form-group">
                <label>Thời gian thanh toán:</label>
                <label><?php echo $_GET['vnp_PayDate'] ?></label>
            </div>
            <div class="form-group">
                <label>Kết quả:</label>
                <label>
                    @if ($secureHash == $vnp_SecureHash)
                        @if ($_GET['vnp_ResponseCode'] == '00')
                            <span style='color:blue'>GD Thành công</span>;
                        @else
                            <span style='color:red'>GD Không thành công</span>;
                        @endif
                    @else
                        echo <span style='color:red'>Chữ ký không hợp lệ</span>;
                    @endif
                </label>
            </div>
        </div>
        <div>
            <a href="/" class="btn btn-outline-primary w-50">Tiếp tục mua hàng</a>
        </div>
    </div>
@stop
