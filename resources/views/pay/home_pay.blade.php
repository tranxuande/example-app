@extends('layouts.master')
@section('content')
    <div id="pay" class="container-fluid mt-5 mb-5">
        <h3 class="mt-3">Thông tin giao hàng</h3>
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }} text-center">{!! Session::get('message') !!}</p>
            @endif
        </div>

        <form action="" method="POST" id="form-pay" enctype="multipart/form-data" class="mt-2 mb-3" name="form-pay">
            @csrf
            <div class="row">
                <div class="col-md-6" style="background-color: white">

                    <table id="products_views_pay" class="table">
                        <thead>
                        <tr class="text-center">
                            <th>Tên sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Giá tiền</th>
                            <th>Tổng tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div class="form-group row">
                        <label for="full_name" class="col-sm-4 col-form-label">
                            Họ và tên
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="full_name" id="full_name" type="text" class="form-control"
                                   placeholder="Họ và tên"
                                   value="{{isset($_POST['full_name']) ? $_POST['full_name'] : ''}}" required
                                   minlength="5">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone_number" class="col-sm-4 col-form-label">
                            Số điện thoại
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="phone_number" id="phone_number" type="tel"
                                   value="{{isset($_POST['phone_number']) ? $_POST['phone_number'] : ''}}"
                                   class="form-control" placeholder="Số điện thoại" required maxlength="10">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-4 col-form-label">
                            Địa chỉ
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="address" id="address" type="text"
                                   value="{{isset($_POST['address']) ? $_POST['address'] : ''}}"
                                   class="form-control" placeholder="Địa chỉ"  maxlength="250">
                        </div>
                    </div>

                    <div>
                        <input name="card_items" id="card_items" type="hidden" value="">
                    </div>
                    <div>
                        <input name="note" id="note" type="hidden" value="">
                    </div>
                    <div class="text-lg-right mt-5">
                        {{--                        <a href="payment-methods" class="btn btn-primary pr-5 pl-5 mr-3">Thanh toán</a>--}}
                        <a href="/cart-display" class="btn btn-primary pr-5 pl-5 mr-3">Quay lại giỏ hàng</a>
                        <a href="/vn-pay" type="submit" class="btn btn-primary pr-5 pl-5 mr-3">Thanh toán VNPay</a>
                        <button type="submit" id="payment_confirmation" class="confirm btn btn-primary pr-5 pl-5">Xác nhận</button>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="mb-4">
                        <h4>Phương thức vận chuyển</h4>
                        <div class="content-box">
                            <div class="radio-input ml-5">
                                <i class="fas fa-dot-circle"></i>
                                <span class="radio-label">Giao hàng tận nơi có tính phí</span>
                            </div>
                        </div>

                        <h4 class="mt-3">Phương thức thanh toán</h4>
                        <div class="content-box-2">
                            <div class="radio-input-2 ml-5">
                                <div>
                                    <i class="fas fa-dot-circle"><img src="/images/Tienmat_Icon_big-2.png" alt=""
                                                                      style="margin-left: 16px; width: 110px;"></i>
                                    <span class="radio-label-2">Thanh toán khi giao hàng (COD)</span>
                                </div>
                            </div>
                            <div>
                                <h6 style="margin-left: 94px;">
                                    Để hỗ trợ tốt cho khách trong việc nhận hàng mà bị THIẾU SAI HÀNG, thì quý khách vui
                                    lòng quay lại video lúc nhận hàng. Xin cảm ơn
                                </h6>
                            </div>
                        </div>
                        <div class="content-box-2">
                            <div class="radio-input-2 ml-5">
                                <div>
                                    <i class="fas fa-dot-circle"><img src="/images/chuyen-khoan.png" alt=""
                                                                      style="margin-left: 16px; width: 110px;"></i>
                                    <span class="radio-label-2">Chuyển khoản qua ngân hàng</span>
                                </div>
                            </div>
                            <div>
                                <h6 style="margin-left: 94px;">
                                    > Với đơn hàng thanh toán qua chuyển khoản quý khách vui lòng CHUYỂN KHOẢN NHANH
                                    24/7 vào <b style="color: #1f6fb2">STK ngân hàng BIDV:21510003936923</b> và nội dung
                                    chuyển khoản CHỈ GHI đúng “HỌ và tên ĐẦY ĐỦ khách hàng”.
                                </h6>
                                <h6 style="margin-left: 94px;">
                                    > Ngay khi nhận được báo “Có” từ Ngân hàng, chúng tôi sẽ tiến hành xác nhận với quý
                                    khách và xuất hàng giao hàng cho quý khách trong thời gian quy định.
                                </h6>
                            </div>
                        </div>
                        {{--                        <div class="content-box-2">--}}
                        {{--                            <div class="radio-input-2 ml-5" >--}}
                        {{--                                <div>--}}
                        {{--                                    <i class="fas fa-dot-circle"></i>--}}
                        {{--                                    <span class="radio-label-2">--}}
                        {{--                                        Sau khi xác nhận mua hàng bạn hãy liên hệ với cửa hàng qua:--}}
                        {{--                                        <br>--}}
                        {{--                                        <b style="margin-left: 28px;">Số điện thoại:0967894659</b>--}}
                        {{--                                        <br>--}}
                        {{--                                        <b style="margin-left: 28px;">Gmail:daoquanghuythanhhai@gmail.com</b>--}}
                        {{--                                        <br>--}}
                        {{--                                        <b style="margin-left: 28px;">Hoặc nhắn tin với cửa hàng bằng Messenger</b>--}}
                        {{--                                    </span>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>


@stop
