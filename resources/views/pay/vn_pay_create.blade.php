@extends('layouts.master')
@section('content')
    <div class="container mt-5">
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }} text-center">{!! Session::get('message') !!}</p>
            @endif
        </div>
        <div class="header clearfix">
            <h3 class="text-muted">VNPAY</h3>
        </div>
        <h3>Tạo mới đơn hàng</h3>
        <div class="table-responsive">
            <form action="/vnpay-create-payment" id="create_form" method="post"  enctype="multipart/form-data">
                {!! csrf_field() !!}
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="full_name">
                                Họ và tên
                            </label>
                            <input name="full_name" id="full_name" type="text" class="form-control"
                                   placeholder="Họ và tên"
                                   value="{{isset($_POST['full_name']) ? $_POST['full_name'] : ''}}"
                                   required minlength="5">
                        </div>
                        <div class="form-group">
                            <label for="phone_number">
                                Số điện thoại
                            </label>
                            <input name="phone_number" id="phone_number" type="tel"
                                   value="{{isset($_POST['phone_number']) ? $_POST['phone_number'] : ''}}"
                                   class="form-control" placeholder="Số điện thoại" required maxlength="10">
                        </div>
                        <div class="form-group ">
                            <label for="address">
                                Địa chỉ
                            </label>
                            <input name="address" id="address" type="text"
                                   value="{{isset($_POST['address']) ? $_POST['address'] : ''}}"
                                   class="form-control" placeholder="Địa chỉ" required maxlength="250">
                        </div>

                        <div class="form-group">
                            <label for="language">Loại hàng hóa <i class="text-danger"></i></label>
                            <select name="order_type" id="order_type" class="form-control">
                                <option value="billpayment">Thanh toán hóa đơn</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="order_id">Mã hóa đơn</label>
                            <input class="form-control" id="order_id" name="order_id" type="text"
                                   value="MHD-<?php echo date("YmdHis") ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="amount">
                                Số tiền<i class="text-danger">(Khách hàng nhập đúng số tiền cần thanh toán)</i>
                            </label>
                            <input class="form-control" id="amount"
                                   name="amount" type="number" value="{{isset($_POST['amount']) ? $_POST['amount'] : ''}}" required>
                        </div>
                        <div class="form-group">
                            <label for="order_desc">Nội dung thanh toán</label>
                            <textarea class="form-control" cols="20" id="order_desc" name="order_desc" rows="2" required>{{ old('order_desc')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="bank_code">Ngân hàng</label>
                            <select name="bank_code" id="bank_code" class="form-control">
                                <option value="">Không chọn</option>
                                <option value="NCB"> Ngan hang NCB</option>
                                <option value="AGRIBANK"> Ngan hang Agribank</option>
                                <option value="SCB"> Ngan hang SCB</option>
                                <option value="SACOMBANK">Ngan hang SacomBank</option>
                                <option value="EXIMBANK"> Ngan hang EximBank</option>
                                <option value="MSBANK"> Ngan hang MSBANK</option>
                                <option value="NAMABANK"> Ngan hang NamABank</option>
                                <option value="VNMART"> Vi dien tu VnMart</option>
                                <option value="VIETINBANK">Ngan hang Vietinbank</option>
                                <option value="VIETCOMBANK"> Ngan hang VCB</option>
                                <option value="HDBANK">Ngan hang HDBank</option>
                                <option value="DONGABANK"> Ngan hang Dong A</option>
                                <option value="TPBANK"> Ngân hàng TPBank</option>
                                <option value="OJB"> Ngân hàng OceanBank</option>
                                <option value="BIDV"> Ngân hàng BIDV</option>
                                <option value="TECHCOMBANK"> Ngân hàng Techcombank</option>
                                <option value="VPBANK"> Ngan hang VPBank</option>
                                <option value="MBBANK"> Ngan hang MBBank</option>
                                <option value="ACB"> Ngan hang ACB</option>
                                <option value="OCB"> Ngan hang OCB</option>
                                <option value="IVB"> Ngan hang IVB</option>
                                <option value="VISA"> Thanh toan qua VISA/MASTER</option>
                            </select>
                        </div>
                                        <div class="form-group">
                                            <label for="language">Ngôn ngữ</label>
                                            <select name="language" id="language" class="form-control">
                                                <option value="vn">Tiếng Việt</option>
                                                <option value="en">English</option>
                                            </select>
                                        </div>
                        <div class="form-group d-none">
                            <label>Thời hạn thanh toán</label>
                            <input class="form-control" id="txtexpire"
                                   name="txtexpire" type="text" value="{{$expire}}"/>
                        </div>
                        <div>
                            <input name="card_items" id="card_items" type="hidden" value="">
                        </div>
                        <div class="">
                            <a href="/pay" class="btn btn-outline-primary w-25">Quay lại</a>
                            <button type="submit" name="redirect" id="redirect" class="btn btn-outline-primary ml-3 w-50">Thanh toán</button>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <table id="products_views_pay" class="table">
                            <thead>
                            <tr class="text-center">
                                <th>Tên sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Giá tiền</th>
                                <th>Tổng tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
