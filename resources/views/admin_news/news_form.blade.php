@extends('layouts_admin.master')
@section('content')
    <div id="creat-news" class="container-fluid">
        <h4>THÔNG TIN BÀI VIẾT</h4>
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('message') !!}</p>
            @endif
        </div>
        <form action="{{ isset($news) ? '/news/' . $news->id  : route('news.store') }}" method="POST"  enctype="multipart/form-data">
            @csrf
            @if (isset($news))
                {{ method_field('PUT') }}
                <input type="hidden" name="id" value="{{$news->id}}">
            @endif
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group row">
                        <label for="title" class="col-sm-4 col-form-label">
                            Tên bài viết
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="title" id="title" type="text"
{{--                                   value="{{old('title')}}"--}}
                                   value="{{ old('title', isset($news->title) ? $news->title : '') }}"

                                   class="form-control" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Công khai</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="publish" value="0">
                            <input id="publish" name="publish" type="checkbox"
                                   value="1" {{ !!old('publish') || (isset($news->publish) && !!$news->publish) ? 'checked="checked"' : '' }} >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">
                            Ảnh bài viết
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <div class="row">
                                @if(isset($news->link_image))
                                    <div class="col-md-3 mb-2 img_product1">
                                        <img src="{{asset($news->link_image)}}" alt="">
                                    </div>
                                @endif
                            </div>
                            <input class="mb-2" type="file" name="photos" multiple>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group row">
                        <label for="content" class="col-sm-12 col-form-label">Bài viết<span class="text-danger">*</span></label>
                        <div class="col-sm-12">
                            <textarea id="content" name="content" class="form-control"
                                      rows="3">
                                {{ old('content', isset($news) ? $news->content : '')}}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary w-25">Xác nhận</button>
            </div>
        </form>
    </div>

@stop
@section('scripts')
    <script>
        tinymce.init({
            selector: 'textarea#content',
            height: 300,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>
@stop
