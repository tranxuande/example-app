@extends('layouts.master')
@section('content')
    <div class="container-fluid " id="introduce">
        <div class="block-about-top mr-4 ml-4">
            <img  src="/images/img/CNF00135-_1_.jpg" alt="">
        </div>
        <div class="block-about-text container ">
            <h1 class="title text-center mb-5">Fashion Store - Khoác lên niềm vui gia đình Việt</h1>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-1 text-center">
                    <p><img src="/images/tao-logo-shop-quan-ao-ny.jpg" alt=""></p>
                </div>
                <div class="col-sm-6 col-sm-offset-1 ">
                    <p><i>Fashion Store - Khoác lên niềm vui gia đình Việt</i></p>
                    <p>Năm 2020, Công ty Cổ phần Thương mại và Dịch vụ Hoàng Dương được thành lập với mục đích chính ban đầu là hoạt động trong lĩnh vực sản xuất hàng thời trang xuất khẩu với các sản phẩm chủ yếu làm từ len và sợi.</p>
                    <p>Năm 2021 thương hiệu thời trang FASHION ra đời, tự hào trở thành một cột mốc đáng nhớ của doanh nghiệp Việt trong ngành thời trang.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-pull-4">
                    <h2 class="title">Tầm nhìn và sứ mệnh</h2>
                    <p><i>Mang đến niềm vui cho hàng triệu gia đình Việt</i> </p>
                    <p>Fashion hướng đến mục tiêu mang lại niềm vui mặc mới mỗi ngày cho hàng triệu người tiêu dùng Việt. Chúng tôi tin rằng người dân Việt Nam cũng đang hướng đến một cuộc sống năng động, tích cực hơn.</p>
                </div>
                <div class="col-sm-6 col-sm-push-6">
                    <p><img src="/images/img/Canifa-Casual-20208014-_1_.jpg" alt=""></p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-offset-1 mb-5">
                <h2>Giá trị cốt lõi của Fashion Store</h2>
                <p><i>Fashion - Chúng tôi luôn tuân thủ những giá trị cốt lõi của mình.</i></p>
                <h3>Kinh doanh dựa trên giá trị thật:</h3>
                <p>FASHION STORE thiết lập hệ thống tiêu chuẩn chất lượng quốc tế áp dụng trên tất cả quy trình quản lý và kiểm soát chất lượng từ khâu chọn lọc nguyên phụ liệu cho đến khâu thiết kế và sản xuất (Oeko-tex, Cotton USA, Woolmark,...).</p>
                <h3>Canifa cam kết phát triển xanh cùng người Việt bằng suy nghĩ và hành động:</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-1">
                    <p><img src="/images/img/Anh_gioi_thieu-02.jpg" alt=""></p>
                </div>
                <div class="col-sm-6 col-sm-offset-1 ">
                    <p><b><i>Vận hành xanh: </i></b>Tổ hợp FASHION STORE tự hào là một đơn vị tiên phong nhận chứng chỉ quốc tế LEED về tiết kiệm năng lượng và ảnh hưởng tích cực đến môi trường sống.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-pull-4">
                    <p><b><i>Đối tác xanh: </i></b>Fashion Store chọn Cotton USA - đơn vị cung cấp nguyên liệu chính cho sản phẩm tại FASHION, luôn nghiêm minh tuân thủ các chỉ số bền vững của nông nghiệp Mỹ: tiết kiệm nước, kỹ thuật “không làm đất” để bảo vệ đất trồng.</p>
                </div>
                <div class="col-sm-6 col-sm-push-6">
                    <p><img src="/images/img/Anh_gioi_thieu-01.jpg" alt=""></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-1">
                    <p><img src="/images/img/len-long-cuu-uc-wool-mark.jpg" alt=""></p>
                </div>
                <div class="col-sm-6 col-sm-offset-1 ">
                    <p><b><i>Sản phẩm xanh: </i></b>FASHION STORE đặc biệt chú trọng nghiên cứu, kiểm định chất lượng với nguyên liệu đầu vào và sản phẩm đầu ra, đáp ứng những yêu cầu khắt khe nhất của các chứng chỉ uy tín nhất thế giới (Oeko Tex, Woolmark, WD…)</p>
                </div>
            </div>
        </div>
    </div>

@stop
