@extends('layouts.master')
@section('content')
    <div id="product_list" class="container mt-4 mb-4">
        <div class="row">
         @include('product_list.product_list_left')
         @include('product_list.product_list_right')
        </div>
    </div>
@stop
