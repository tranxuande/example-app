<!DOCTYPE html>
<html lang="vi">
<head>
    <title>Admin</title>
    @include('layouts_admin.head_tag')
</head>
<body>
<div class="container-login">
    <div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
    </div>
    <form method="post" action="/login" class="form-1">
        @csrf
        <h1>Login</h1>
        <label for="username">Tài khoản</label>
        <input type="text" name="username" required/>
        <label for="password">Mật khẩu</label>
        <input type="password" name="password" id="password" required/>
        <button class="button-login">Đăng nhập</button>
    </form>
</div>
</body>
</html>
