@extends('layouts_admin.master')
@section('content')
    <div id="home-oder" class="container-fluid">
        <div class="title mb-2">
            <h4>Danh sách đơn hàng</h4>
            <div class="btn-create-products">
                <a href="/orders/create" class="creat btn btn-primary">
                    Tạo sản phẩm mới
                </a>
            </div>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('message') !!}</p>
        @endif
        <div>
            <form class="row mb-2" action="">
                <div class="col-md-2">
                    <label for="code_orders">Mã đơn hàng</label>
                    <input type="text" name="code_orders" id="code_orders"
                           value="{{isset($_GET['code_orders']) ? $_GET['code_orders'] : ''}}"
                           class="form-control form-control-sm">
                </div>
                <div class="col-md-2">
                    <label for="full_name">Họ và tên</label>
                    <input type="text" name="full_name" id="full_name"
                           value="{{isset($_GET['full_name']) ? $_GET['full_name'] : ''}}"
                           class="form-control form-control-sm">
                </div>
                <div class="col-md-2">
                    <label for="phone_number">Số điện thoại</label>
                    <input type="number" name="phone_number" id="phone_number"
                           value="{{isset($_GET['phone_number']) ? $_GET['phone_number'] : ''}}"
                           class="form-control form-control-sm">
                </div>
                <div class="col-md-2">
                    <label for="address">Địa chỉ</label>
                    <input type="text" name="address" id="address"
                           value="{{isset($_GET['address']) ? $_GET['address'] : ''}}"
                           class="form-control form-control-sm">
                </div>
                <div class="col-md-2">
                    <label for="status">Trạng thái</label>
                    <select name="status" id="status" class="form-control">
                        <option value="">Tất cả</option>
                        @foreach($orders_status_name as $key => $value)
                            <option
                                value="{{$key}}" {{isset($_GET['status']) && $_GET['status'] == $key ? 'selected' : ''}}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="date_from">Ngày tạo từ</label>
                    <input type="date" class="form-control form-control-sm" name="date_from" id="date_from"
                           value="{{isset($_GET['date_from']) ? $_GET['date_from'] : ''}}">
                </div>
                <div class="col-md-2">
                    <label for="date_to">Ngày tạo đến</label>
                    <input type="date" class="form-control form-control-sm" name="date_to" id="date_to"
                           value="{{isset($_GET['date_to']) ? $_GET['date_to'] : ''}}">
                </div>
                <div class="col-md-2">
                    <label class="w-100" for="" style="opacity: 0">Tìm kiếm</label>
                    <button type="submit" class="btn btn-primary w-100">Tìm kiếm</button>
                </div>
            </form>
        </div>
        <div class="row  mt-3">
            <div class="col-sm-3">
                <h4>
                    Tổng: {{$total_orders->total_orders}} đơn hàng
                </h4>
            </div>
            <div class="col-sm-4">
                <h4>
                    Đơn hàng đã hoàn thành: {{$orders_completed_order->orders_completed_order}} đơn hàng
                </h4>
            </div>
            <div class="col-sm-5">
                <h4>
                    Tổng tiền sản phẩm đã bán được: {{number_format($total_order_amount->total_order_amount)}} VND
                </h4>
            </div>
        </div>

        <div class="table-responsive-sm">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Mã đơn hàng</th>
                    <th>Họ và tên</th>
                    <th>Số điện thoại</th>
                    <th>Địa chỉ</th>
                    <th>Ghi chú</th>
                    <th style="width: 161px;">Trạng thái</th>
                    <th>Lí do hủy</th>
                    <th>Ngày tạo</th>
                    <th>Ngày cập nhập</th>
                    <th style="width: 80px"></th>
                    <th style="width: 80px"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $key => $value)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$value->code_orders}}</td>
                        <td>{{$value->full_name}}</td>
                        <td>{{$value->phone_number}}</td>
                        <td>{{$value->address}}</td>
                        <td>{{$value->note}}</td>
                        <td>{{$orders_status_name[$value->order_status_id]}}</td>
                        <td>{{$value->cancellation_reason}}</td>
{{--                        <td>{{isset($orders_status_name[$value->order_status_id]) ? $orders_status_name[$value->order_status_id] : 'Chờ'}}</td>--}}

{{--                        @if($value->order_status_id == 5 )--}}
{{--                            <td>--}}
{{--                                {{$orders_status_name[$value->order_status_id]}}--}}
{{--                            </td>--}}
{{--                            <td></td>--}}
{{--                        @else--}}
{{--                            <td >--}}
{{--                                <select class="select form-control-sm" style="width: 75px;" orders_id="{{$value->id}}">--}}
{{--                                    @foreach($orders_status as $key => $item)--}}
{{--                                        @if($key + 1 == $value->order_status_id || $key == $value->order_status_id || $key == 5)--}}
{{--                                            <option class="status" value="{{$key+1}}"  >--}}
{{--                                                {{$item->status}}--}}
{{--                                            </option>--}}
{{--                                        @endif--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                                <button type="submit" class="update_btn btn-sm btn-primary w-80" >--}}
{{--                                    <i class="fas fa-upload "></i> Sửa--}}
{{--                                </button>--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                <input type="text" class="cancellation_reason form-control-sm" input_orders_id="{{$value->id}}" value="" disabled>--}}
{{--                            </td>--}}
{{--                        @endif--}}

                        <td>{{date('d-m-Y H:i:s', strtotime($value->created_at))}}</td>
                        <td>{{date('d-m-Y H:i:s', strtotime($value->update_at))}}</td>
                        <td style="white-space: nowrap">
                            <a class="btn btn-sm btn-primary" href="/orders/{{$value->id}}/edit"><i
                                    class="far fa-edit"></i> Sửa</a>
                        </td>
                        <td>
                            <form action="{{ url('/orders', ['id' => $value->id]) }}" method="post" onsubmit="return confirm('Bạn chắc chắn muốn xóa sản phẩm này không?');">
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-sm btn-danger"><i class="far fa-trash-alt"></i> Xóa</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div>
            @include('pagination.default_2', ['paginator' => $orders])
        </div>
    </div>

@stop
