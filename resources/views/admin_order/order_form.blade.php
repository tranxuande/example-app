@extends('layouts_admin.master')
@section('content')
    <div id="creat-oder" class="container-fluid">
        <h4>Thông tin giao hàng</h4>
        <div>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{!! Session::get('message') !!}</p>
            @endif
        </div>

        <form action="{{ isset($orders) ? '/orders/' . $orders->id  : route('orders.store') }}" method="POST"  enctype="multipart/form-data">
            @csrf
            @if (isset($orders))
                {{ method_field('PUT') }}
                <input type="hidden" name="id" value="{{$orders->id}}">
            @endif
            <div class="row">
                <div class="col-md-7" style="background-color: white">
                    <div class="form-group row">
                        <label for="full_name" class="col-sm-4 col-form-label">
                            Họ và tên
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="full_name" id="full_name" type="text"
                                   value="{{ isset($orders->full_name) ? $orders->full_name : '' }}"
                                   class="form-control" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone_number" class="col-sm-4 col-form-label">
                            Số điện thoại
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="phone_number" id="phone_number" type="number"
                                   value="{{ isset($orders->phone_number) ? $orders->phone_number : '' }}"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-4 col-form-label">
                            Địa chỉ
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input name="address" id="address" type="text"
                                   value="{{ isset($orders->address) ? $orders->address : '' }}"
                                   class="form-control" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="note" class="col-sm-4 col-form-label">
                            Ghi chú
                            <span class="text-danger"></span>
                        </label>
                        <div class="col-sm-8">
                            <input name="note" id="note" type="text"
                                   value="{{ isset($orders->note) ? $orders->note : '' }}"
                                   class="form-control" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-sm-4 col-form-label">Trạng thái<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="order_status_id" id="status" class="form-control" required>
                                <option value="">Chọn danh mục</option>
                                @foreach($orders_status_name as $key => $value)
                                    <option
                                        value="{{$key}}" {{old('order_status_id') == $key || (isset($orders) && $orders->order_status_id == $key) ? 'selected' : ''}}>{{$value}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="reason" class="col-sm-4 col-form-label">
                            Lí do hủy
                            <span class="text-danger"></span>
                        </label>
                        @if((isset($orders) && $orders->order_status_id == '6'))
                            <div class="col-sm-8">
                                <input name="cancellation_reason" id="reason" type="text"
                                       value="{{ isset($orders->cancellation_reason) ? $orders->cancellation_reason : '' }}"
                                       class="form-control" >
                            </div>
                        @else
                            <div class="col-sm-8">
                                <input name="cancellation_reason" id="reason" type="text"
                                       value="{{ isset($orders->cancellation_reason) ? $orders->cancellation_reason : '' }}"
                                       class="form-control" disabled>
                            </div>
                        @endif
                    </div>
                    <div class="text-lg-right">
                        <button type="submit" class="btn btn-primary pr-5 pl-5">Xác nhận</button>
                    </div>
                </div>

                <div class="col-md-5">
                    <div>
                        <table class="table">
                            <thead>
                            <tr class="text-center">
                                <th>Tên sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Giá tiền</th>
                                <th>Tổng tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($orders_details))
                                @foreach($orders_details as $key => $value)
                                    <tr class="text-center">
                                        <td>{{$products[$value->product_id]}}</td>
                                        <td>{{$value->quantity}}</td>
                                        <td>{{number_format($value->money)}}</td>
                                        <td>{{number_format($value->total_money)}}</td>
                                    </tr>
                                @endforeach
                                    <tr>
                                        <th colspan="3" style="font-weight: 600; text-align: right">Tổng cộng:</th>
                                        <td class="text-center">{{number_format($total)}}</td>
                                    </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </form>
    </div>



@stop
