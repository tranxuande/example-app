<section id="section2">
    <div class="container">
        <div class="row">
            <div class=" hover-image col-12 col-sm-6 col-md-4">
                <a class="cms-banner" href="/product-category/thoi-trang-nu" >
                    <img
                        src="/images/thoitrangnu.jpg"
                        alt="Women">
                    <span class="banner-data">
                        <span class="banner-title">Thời trang</span>
                        <span class="banner-subtitle">nữ</span>
                    </span>
                </a>
            </div>
            <div class="hover-image col-12 col-sm-6 col-md-4">
                <a class="cms-banner" href="/product-category/thoi-trang-nam">
                    <img src="/images/thoitrangnam.jpg" alt="Men">
                    <span class="banner-data">
                        <span class="banner-title">Thời trang</span>
                        <span class="banner-subtitle">nam</span>
                    </span>
                </a>

            </div>
            <div class="hover-image col-12 col-sm-6 col-md-4" >
                <a class="cms-banner" href="/product-category/thoi-trang-tre-em">
                    <img src="/images/thoitrangtreem.jpg" alt="Kid">
                    <span class="banner-data">
                        <span class="banner-title">Thời trang</span>
                        <span class="banner-subtitle">trẻ em</span>
                    </span>
                </a>
            </div>

        </div>
    </div>
</section>
