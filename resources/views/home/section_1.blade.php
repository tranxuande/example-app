<section id="section1">
    <div class="container-fluid">
        <div class="row">
            <div id="top-column">
                <div id="myCarousel" class="carousel slide border" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @foreach($slide_image as $key => $value)
                            <li data-target="#myCarousel" data-slide-to="{{$key}}"
                                class="{{ $key == 0 ? 'active' : ''}}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach($slide_image as $key => $value)
                            <div class="carousel-item{{ $key == 0 ? ' active' : ''}}">
                                <a href="/product-show/{{$value->product_link}}">
                                    <img class="d-block w-100" src="{{$value->link_image}}" alt="">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <!-- Controls -->
                    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
