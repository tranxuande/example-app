@extends('layouts.master')
@section('content')
    <div class="container mt-5 mb-5" id="contact">
        <h1>Liên hệ</h1>
        <h3>Hỗ trợ Khách hàng mua online</h3>
        <div class="row">
            <div class="col-sm-6" style="font-size: 16px;">
                <p>Tổng đài: 1800 6061</p>
                <p>Làm việc: 9-17h thứ 2 - 6</p>
            </div>
            <div class="col-sm-6" style="font-size: 16px;">
                <p>
                    Email: daoquanghuythanhhai@gmail.com
                </p>
                <p>
                    Địa chỉ: Phòng 301 Tòa nhà GP Invest,
                    170 La Thành, P. Ô Chợ Dừa, Q. Đống Đa, Hà Nội
                </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-6" style="font-size: 16px;">
                <h3>Chăm sóc khách hàng</h3>
                <p>Điện thoại: 1800.6061</p>
                <p>Email: chamsockhachhang@fashionstore.com</p>
            </div>
            <div class="col-sm-6" style="font-size: 16px;">
                <h3>Nhà máy</h3>
                <p>Đường Nguyễn Văn Linh, Phường Bần Yên Nhân, T.X Mỹ Hào, Hưng Yên</p>
                <p>Điện thoại: +84-221- 394 2234</p>
                <p>Điện thoại: +84-221- 394 2234</p>
            </div>
        </div>
    </div>

@stop
