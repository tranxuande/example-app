@extends('layouts.master')
@section('content')
    <div class="container mt-5 mb-5" id="details_news">
        <div class="row">
            <div class="details-news-right col-md-8">
                <h1 class="post-title ">
                   {{$details_news->title}}
                </h1>
                <div class="post-meta">
                    <span>By FASHION STORE</span>
                    <span class="date">{{date('d/m/Y', strtotime($details_news->update_at))}}</span>
                </div>
                <div>
                    {!! $details_news->content !!}
                </div>
            </div>

            <div class="details-news-left col-md-4">
                <h2 class="text-center">BÀI GẦN ĐÂY</h2>
                <div class="row">
                    @foreach($news as $key => $value)
                        <div class="col-sm-12 mb-4">
                            <div class="post-image">
                                <a href="/details-news/{{$value->link_news}}">
                                    <img src="{{$value->link_image}}">
                                </a>
                            </div>
                            <div class="post-detail">
                                <h3 class="post-title"><a href="/details-news/{{$value->link_news}}">{{$value->title}}</a></h3>
                                <div class="post-date">{{date('d/m/Y', strtotime($value->update_at))}}</div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>

@stop
