@extends('layouts.master')
@section('content')
    <div class="container mb-5 mt-5" id="news" >
        <div class="row">
            @foreach($news as $key => $value)
                <div class="col-md-4">
                <div class="post-image">
                    <a href="/details-news/{{$value->link_news}}">
                        <img src="{{$value->link_image}}">
                    </a>
                </div>
                <div class="post-detail">
                    <h3 class="post-title"><a href="/details-news/{{$value->link_news}}">{{$value->title}}</a></h3>
                    <div class="post-date">{{date('d/m/Y', strtotime($value->update_at))}}</div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="mt-4">
            @include('pagination.default_2', ['paginator' => $news])
        </div>
    </div>

@stop
