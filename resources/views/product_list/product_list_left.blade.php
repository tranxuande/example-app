<div class="col-md-3 mt-3">

    <div>
        <h2>Bộ lọc tìm kiếm</h2>
    </div>
    <form action="" >
        <div class="form-group row">
            <label for="name" class="col-sm-8 col-form-label">
                Tên sản phẩm
            </label>
            <div class="col-sm-12">
                @if(Request::segment(1) == 'product-search')
                    <input name="search" id="name" type="text"
                           value="{{isset($_GET['search']) ? $_GET['search'] : '' }}"
                           class="form-control">

                @else
                    <input name="name" id="name" type="text"
                           value="{{isset($_GET['name']) ? $_GET['name'] : ''}}"
                           class="form-control">
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="price_from" class="col-sm-8 col-form-label" >Giá từ</label>
            <div class="col-sm-12">
                <input type="number" class="form-control form-control-sm" id="price_from" name="price_from"
                       value="{{isset($_GET['price_from']) ? $_GET['price_from'] : ''}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="price_to" class="col-sm-8 col-form-label" >Giá đến</label>
            <div class="col-sm-12">
                <input type="number" class="form-control form-control-sm" id="price_to" name="price_to"
                       value="{{isset($_GET['price_to']) ? $_GET['price_to'] : ''}}">
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary w-100">Tìm kiếm</button>
        </div>
    </form>
</div>
