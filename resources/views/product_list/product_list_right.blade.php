<div class="col-md-9">
    <div class="toolbar mb-4">

        <nav class="navbar navbar-expand-sm ">
            <div class="filter-grid-list">
                Sắp xếp theo
            </div>
            <div class="row">
                <form action="" class="form-inline ml-3" >
                    <input type="hidden" name="sold" value="banchay">
                    <button type="submit" class="btn btn-outline-primary ml-2">Bán chạy</button>
                </form>
                <form  action="" class="form-inline ">
                    <input type="hidden" name="promotion" value="khuyenmai">
                    <button type="submit" class="btn btn-outline-primary ml-2">Khuyến mãi</button>
                </form>
            </div>

        </nav>
    </div>

    <div class="row">
        @foreach($products as $key => $value)
                <div class="list swap-on-hover col-12 col-sm-6 col-md-4 ">
                    <figure class="product-list">
                        <div class="icon_img">
                            <img src="/images/chinh-sach-doi-tra.jpg">
                        </div>
                        <a href="/product-show/{{$value->link}}">
                            <img class="swap-on-hover__front-image"
                                 src="{{isset($product_image_list[$value->id][0]) ? $product_image_list[$value->id][0] : '' }}"
                                 alt="{{$value->name}}" title="{{$value->name}}" >
                            <img class="primary_image lazy loaded"
                                 src="{{isset($product_image_list[$value->id][1]) ? $product_image_list[$value->id][1] : '' }}"
                                 alt="{{$value->name}}" title="{{$value->name}}" >
                        </a>
                        @if($value->percent_discount)
                            <div class="product-discount">
                                {{$value->percent_discount ? $value->percent_discount . '%' : ''}}
                            </div>
                        @endif
                    </figure>
                    <div class="product">
                        <h3 class="product-name">
                            <a href="/product-show/{{$value->link}}">
                                {{$value->name}}
                            </a>
                        </h3>
                        <div class="price-box">
                            @if($value->promotional_price)
                                <span>
                                <bdi>
                                     <span class="brick">
                                         {{number_format($value->price)}}
                                     </span>
                                    <span class="brick">
                                        ₫
                                    </span>
                                </bdi>
                                -
                                <bdi>
                                     <span>
                                         {{number_format($value->promotional_price)}}
                                    </span>
                                    ₫
                                </bdi>
                            </span>
                            @else
                                <span>
                                <bdi>
                                     <span >
                                         {{number_format($value->price)}}
                                     </span>
                                    <span >
                                        ₫
                                    </span>
                                </bdi>
                            </span>
                            @endif
                        </div>
                    </div>
                    <button class="add-product-to-cart btn btn-outline-primary" product_id="{{$value->id}}" name="{{$value->name}}" price="{{$value->price}}" promotional_price="{{$value->promotional_price}}">
                        <span>Thêm vào giỏ hàng</span>
                    </button>
                </div>
            @endforeach
    </div>
    <div class="mt-4">
        @include('pagination.default_2', ['paginator' => $products])
    </div>
</div>
