<!DOCTYPE html>
<html lang="vi">
<head>
    <title>FASHION STORE</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Admin"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="locale" content="{{ App::getLocale() }}"/>
    <link rel="shortcut icon" href={{ asset('favi.png') }} />
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body >
@include('layouts.header')
@yield('content')
@include('layouts.footer')
@yield('scripts')
<script src="{{ mix('/js/app.js') }}"></script>
@yield('plugins')
</body>
</html>
