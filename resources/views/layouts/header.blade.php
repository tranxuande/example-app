<div id="header">
    <div class="container-fluid">
        <header class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light ">

{{--            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">--}}
                <!-- Brand -->
                <a class="navbar-brand" href="/">
                    <img src="{{asset('/images/logo_fashion.jpg')}}" alt="logo">
                </a>

                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="nav nav-tabs">
{{--                    <ul class="nav navbar-nav">--}}
                        <li class="nav-item ">
                            <a class="nav-link {{ request()->is('/*') ? 'active' : '' }}" href="/">Trang chủ <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="category nav-link dropdown-toggle " href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Sản phẩm
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li class="dropdown-submenu">
                                    <a class="dropdown-item dropdown-toggle text-center" href="#">Áo</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item text-center" href="/product-category/ao-nam">Áo nam</a></li>
                                        <li><a class="dropdown-item text-center" href="/product-category/ao-nu">Áo nữ</a></li>
                                        <li><a class="dropdown-item text-center" href="/product-category/ao-tre-em">Áo trẻ em</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a class="dropdown-item dropdown-toggle text-center" href="#">Quần</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item text-center" href="/product-category/quan-nam">Quần nam</a></li>
                                        <li><a class="dropdown-item text-center" href="/product-category/quan-nu">Quần nữ</a></li>
                                        <li><a class="dropdown-item text-center" href="/product-category/quan-tre-em">Quần trẻ em</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link  {{ request()->is('introduce*') ? 'active' : '' }}" href="/introduce">Giới thiệu <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link {{ request()->is('page-news*') ? 'active' : '' }}" href="/page-news">Tin tức</a>
                        </li>
                    </ul>
                </div>
                @if(Request::segment(1) != 'product-search' && Request::segment(1) != 'product-category')
                    <form class="form-inline" action="/product-search" >
                        <input class="form-control mr-sm-2" type="search" name="search" placeholder="Tìm kiếm" aria-label="Search" value="{{isset($_GET['search']) ? $_GET['search'] : ''}}" required maxlength="250" >
                        <button class="btn btn-outline-dark my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                @endif

                <a class="cart btn-outline-dark my-2 my-sm-0 ml-2" href="/cart-display">
                    <i class="fas fa-shopping-cart "></i>
                    <div class="cart-number-badge"></div>
                </a>
            </nav>
        </header>
    </div>
</div>

