<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="description" content="Trang chủ"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="locale" content="{{ App::getLocale() }}"/>
<link rel="shortcut icon" href={{ asset('favi.png') }} />
<link rel="stylesheet" href="{{ mix('/css/app.css') }}">
<script src="{{ mix('/js/app.js') }}"></script>

