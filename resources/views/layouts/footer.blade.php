<footer class="container-fluid">
    <div class="container">
        <div class="row footer-top">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="footer-content1">
                    <h4 class="footer-title">
                        FASHION STORE
                    </h4>
                    <div class="footer-content">
                        <p>
                            Địa chỉ liên hệ: Phòng 301 Tòa nhà GP Invest,
                            170 La Thành, P. Ô Chợ Dừa, Q. Đống Đa, Hà Nội
                        </p>
                        <p>
                            Số điện thoại: 0967894659
                        </p>
                        <p>
                            Địa chỉ email: daoquanghuythanhhai@gmail.com

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="footer-content1">
                    <h4 class="footer-title">
                        Thông tin
                    </h4>
                    <div class="footer-content toggle-footer">
                        <ul>
                            <li class="item">
                                <a href="/introduce" title="Giới thiệu">Giới thiệu</a>
                            </li>
                            <li class="item">
                                <a href="/page-news" title="Liên hệ">Tin  tức</a>
                            </li>
{{--                            <li class="item">--}}
{{--                                <a href="#" title="Tài khoản">Tài khoản</a>--}}
{{--                            </li>--}}
{{--                            <li class="item">--}}
{{--                                <a href="#" title="Cửa hàng">Cửa hàng</a>--}}
{{--                            </li>--}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="footer-content1">
                    <h4 class="footer-title">
                        Chính sách
                    </h4>
                    <div class="footer-content toggle-footer">
                        <ul>
                            <li class="item">
                                <a href="/contact" title="Hướng dẫn mua hàng">Liên hệ</a>
                            </li>
                            <li class="item">
                                <a  href="/return-policy" title="Quy định đổi hàng">Quy định đổi hàng</a>
                            </li>
{{--                            <li class="item">--}}
{{--                                <a  href="#" title="Tuyển dụng">Chính sách vận chuyển</a>--}}
{{--                            </li>--}}
{{--                            <li class="item">--}}
{{--                                <a  href="#" title="Hợp tác kinh doanh">Hợp tác kinh doanh</a>--}}
{{--                            </li>--}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="footer-content1">
                    <h4 class="footer-title">
                        Kết nối với fashion store
                    </h4>
                    <div class="">
                        <a href="#" target=" _blank"><i class="fab fa-facebook-square fa-3x" style="color: #1d2124"></i></a>
                        <a href="#" target=" _blank"><i class="fab fa-instagram-square fa-3x ml-3" style="color: #1d2124"></i></a>
                    </div>
                </div>
            </div>

        </div>
        <div class="footer-bottom">© 2021 ĐÀO QUANG HUY TRƯỜNG ĐẠI HỌC ĐIỆN LỰC</div>
    </div>
</footer>
