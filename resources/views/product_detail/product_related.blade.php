<div class="container">
    <div>
        <h2 class="home-title">
            <span>SẢN PHẨM LIÊN QUAN</span>
        </h2>
    </div>
    <div class="row">
        @foreach($products_related as $key => $value)
            @include('product_detail.product_item', ['value' => $value, 'images' => $product_image_related])
        @endforeach
    </div>
</div>
