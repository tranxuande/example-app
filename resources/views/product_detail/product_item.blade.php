<div class="related swap-on-hover col-12 col-sm-6 col-md-3 ">
    <figure class="product-related">
        <div class="icon_img">
            <img src="/images/chinh-sach-doi-tra.jpg">
        </div>
        <a href="/product-show/{{$value->link}}">
            <img class="swap-on-hover__front-image"
                 src="{{isset($images[$value->id][0]) ? $images[$value->id][0] : '' }}"
                 alt="{{$value->name}}" title="{{$value->name}}">
            <img class=""
                 src="{{isset($images[$value->id][1]) ? $images[$value->id][1] : '' }}"
                 alt="{{$value->name}}" title="{{$value->name}}">
        </a>
        @if($value->percent_discount)
            <div class="product-discount">
                {{$value->percent_discount ? $value->percent_discount . '%' : ''}}
            </div>
        @endif
    </figure>
    <div class="product">
        <h3 class="product-name">
            <a href="/product-show/{{$value->link}}">
                {{$value->name}}
            </a>
        </h3>
        <div class="price-box">
            @if($value->promotional_price)
                <span>
                    <bdi>
                         <span class="brick">
                             {{number_format($value->price)}}
                         </span>
                        <span class="brick">
                            ₫
                        </span>
                    </bdi>
                    -
                    <bdi>
                         <span>
                             {{number_format($value->promotional_price)}}
                        </span>
                        ₫
                    </bdi>
                </span>
            @else
                <span>
                    <bdi>
                         <span >
                            {{number_format($value->price)}}
                         </span>
                        <span >
                            ₫
                        </span>
                    </bdi>
                </span>
            @endif

        </div>
    </div>
    <button class="add-product-to-cart btn btn-outline-primary" product_id="{{$value->id}}" name="{{$value->name}}" price="{{$value->price}}" promotional_price="{{$value->promotional_price}}">
        <span>Thêm vào giỏ hàng</span>
    </button>
</div>
