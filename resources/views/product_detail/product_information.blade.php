<div class="container">
    <div class="row">
        <div class="col col-md-5">
            <div class="img_top">
                <figure class="zoom">
                    <img class="img_detail"
                         src="{{isset($product_image_db_detail[0]) ? $product_image_db_detail[0] : '' }}" id="picture">
                </figure>
                <div class="product-discount">
                    {{$products->percent_discount ? $products->percent_discount . '%' : ''}}
                </div>
            </div>

            <div class="row mt-2 ">
                @foreach($product_image_db_detail as $key => $value)
                    <div class="div col-md-3 col-sm-3 col-3 mb-2">
                        <img
                            src="{{$value}}"
                            alt="" class="image" >
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-7">
            <h1 class="product-title entry-title">{{$products->name}}</h1>
{{--            <div class="form-group row">--}}
{{--                <label for="" class="col-sm-2 col-form-label">Đánh giá</label>--}}
{{--                <div class="col-sm-10 mt-2">--}}
{{--                    <div class="stars">--}}
{{--                        <form action="">--}}
{{--                            <i class="fas fa-star"></i>--}}
{{--                            <i class="fas fa-star"></i>--}}
{{--                            <i class="fas fa-star"></i>--}}
{{--                            <i class="fas fa-star"></i>--}}
{{--                            <i class="fas fa-star"></i>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Giá tiền</label>
                <div class="col-sm-10 mt-2">
                    <div class="price-box ">
                                    <span>
                                        <bdi>
                                             <span class="brick">
                                                 {{number_format($products->price)}}
                                             </span>
                                            <span class="brick">
                                                ₫
                                            </span>
                                        </bdi>
                                        -
                                        <bdi>
                                             <span>
                                                 {{number_format($products->promotional_price)}}
                                            </span>
                                            ₫
                                        </bdi>
                                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Mô tả</label>
                <div class="product-description mt-2 col-sm-10">
                    <p>
                        {{$products->description}}
                    </p>
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Có sẵn</label>
                <div class="product-description mt-2 col-sm-10">
                    <p>
                        {{$products->quantily}} sản phẩm
                    </p>
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Đã bán</label>
                <div class="product-description mt-2 col-sm-10">
                    <p>
                        {{$products->sold}} sản phẩm
                    </p>
                </div>
            </div>
{{--            <div class="form-group row">--}}
{{--                <label for="size" class="col-sm-2 col-form-label">Kích cỡ</label>--}}
{{--                <div class="col-sm-5 mt-2">--}}
{{--                    <select name="" id="size" class="form-control">--}}
{{--                        <option>Chọn kích cỡ</option>--}}
{{--                        <option>X</option>--}}
{{--                        <option>XX</option>--}}
{{--                        <option>XXL</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Số lượng</label>
                <div class="col-sm-5 mt-2">
                    <div class="buttons_added">
                        <input class="quantity form-control" max="Số tối đa" min="Số tối thiểu" name="" type="number" value="1">
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <button class="add-product-to-cart btn btn-block btn-outline-primary " product_id="{{$products->id}}" name="{{$products->name}}" price="{{$products->price}}" promotional_price="{{$products->promotional_price}}">Thêm vào giỏ hàng</button>
                    <a href="/cart-display" class="add-product-to-cart btn btn-block btn-primary" product_id="{{$products->id}}" name="{{$products->name}}" price="{{$products->price}}" promotional_price="{{$products->promotional_price}}">Mua ngay</a>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container tabs-content">
    <ul class="">
        <li class="active " id="tab-title-description" aria-controls="tab-description" role="tab">
            <a href="#tab-description">
                Chi tiết
            </a>
        </li>
    </ul>
    <div id="tab-description" role="tabpanel" aria-labelledby="tab-title-description">
        {!! $products->content !!}
    </div>
</div>
