// $(".add-product-to-cart").click(function () {
//     // lấy giá trị khi click
//     let product_id = $(this).attr("product_id");
//     let name = $(this).attr("name");
//     let price = $(this).attr("price");
//     let promotional_price = $(this).attr("promotional_price");
//     //tạo đối tượng cho product_id
//     let obj = {};
//     //nếu có giá khuyến mại thì lấy giá
//     if (promotional_price) {
//         obj['price'] = promotional_price;
//     } else {
//         obj['price'] = price;
//     }
//     let quantity = 1;
//     obj['product_id'] = product_id;
//     obj['name'] = name;
//     obj['quantity'] = quantity;
//     // lấy card_items cũ và check có hay không card_items cũ
//     let old_cart = localStorage.getItem("card_items");
//     // nếu có card_items cũ thì obj_2=card_items cũ nếu không obj_2 ={}
//     let obj_2 = {};
//     if (old_cart) {
//         // chuyển string sang object
//         old_cart = JSON.parse(old_cart);
//         obj_2 = old_cart;
//     }
//     //đẩy đối tượng product_id vào đối tượng chung
//     obj_2[product_id] = obj;
//     //chuyển object sang string
//     obj_2 = JSON.stringify(obj_2);
//     // đẩy obj_2 vào localstorage với key = card_items
//     localStorage.setItem("card_items", obj_2);
// });
// Thêm sản phẩm vào giỏ hàng
$(".add-product-to-cart").click(function () {
    // lấy giá trị khi click
    let product_id = $(this).attr('product_id');
    let name = $(this).attr('name');
    let price = $(this).attr('price');
    let promotional_price = $(this).attr('promotional_price');
    let quantity = 1;
    if ($('input.quantity').length > 0) {
        quantity = $('.quantity').val();
        if (!quantity) {
            quantity = 1;
        }
    }
    //tạo đối tượng cho product_id ...
    let obj = {};
    obj['product_id'] = product_id;
    obj['name'] = name;
    obj['quantity'] = quantity;
    //nếu có giá khuyến mại thì lấy giá khuyến mại nếu không có lấy giá gốc
    if (promotional_price) {
        obj['price'] = promotional_price;
    } else {
        obj['price'] = price;
    }
    let obj_2 = {};
    // lấy giá trị card_items cũ từ localStorage và check có hay không card_items cũ
    let old_cart = localStorage.getItem("card_items");
    // nếu có old_cart  thì obj_2=old_cart nếu không obj_2 ={}
    if (old_cart) {
            // chuyển string sang object
            old_cart = JSON.parse(old_cart);
            obj_2 = old_cart;
        }
    obj_2[product_id] = obj;
    // chuyển object sang string
    obj_2 = JSON.stringify(obj_2);
    // đẩy obj_2 vào localstorage với key = card_items
    localStorage.setItem("card_items", obj_2);
    showCartCount();
});

// Hiển thị số lượng sản phẩm trong giỏ hàng
function showCartCount() {
    let count = 0;
    let data = localStorage.getItem("card_items");
    if (data) {
        // chuyển string sang object
        data = JSON.parse(data);
        for (let i in data) {
            count++;
        }
    }
    // hiển thị ra html
    $(".cart-number-badge").html(count);
}

// Chạy javascript
$(document).ready(function () {
    showCartCount();
    showProductsInCart();
    display_pay();
    // check path
    let pathname = window.location.pathname;
    if (pathname === '/pay') {
        showProductInPay();
        let card_items = localStorage.getItem('card_items') || '';
        $('#card_items').val(card_items);

        let note = localStorage.getItem('note') || '';
        $('#note').val(note);
    }
    if (pathname === '/vn-pay') {
        showProductInPay();
        let card_items = localStorage.getItem('card_items') || '';
        $('#card_items').val(card_items);
    }

    if (pathname === '/vnpay-return') {
        localStorage.clear();
        showCartCount();
    }
    if (pathname === '/payment-methods') {
        localStorage.clear();
        showCartCount();
    }
});

// Hiển thị sản phẩm trong giỏ hàng
function showProductsInCart() {
    let data = localStorage.getItem("card_items");
    if (data) {
        // chuyển string sang object
        data = JSON.parse(data);
        let sum = 0;
        let html = '';
        // Dùng foreach
        $.each(data, function (index, value) {
            sum += (parseInt(value.price) * parseInt(value.quantity));
            let total = (parseInt(value.price) * parseInt(value.quantity));
            html += '<tr>';
            html += '<td style="text-transform: capitalize;">' + value.name + '</td>';
            html += '<td>' + '<input class="change-quantity-product-cart" product_id="' + value.product_id + '" type="number" min="1"  value="' + value.quantity + '">' + '</td>';
            html += '<td>' + Number(value.price).toLocaleString() + '<span>' + '₫' + '</span>' + '</td>';
            html += '<td>' + Number(total).toLocaleString() + '<span>' + '₫' + '</span>'  + '</td>';
            html += '<td class="text-center"><button type="submit" class="clear-product-cart btn btn-sm btn-danger" product_id="' + value.product_id+ '"><i class="far fa-trash-alt"></i> Xóa</button></td>';
            html += '</tr>';
        });
        html += '<tr>';
        html += '<td colspan="3" style="font-weight: 600; text-align: right" >' + 'Tổng cộng:' + '</td>';
        html += '<td colspan="2">' + sum.toLocaleString() + '<span>' + '₫' + '</span>'  + '</td>';
        html += '</tr>';
        // hiển thị ra html
        $("#products_views tbody").html(html);

        // Dùng for in
        // for (let i in data) {
        //
        //     let html = '<tr>';
        //     html += '<td>' + '</td>';
        //     html += '<td>' + data[i].name + '</td>';
        //     html += '<td>' + data[i].quantity + '</td>';
        //     html += '<td>' + data[i].price + '</td>';
        //     html += '<td class="text-center"><button type="submit" class="btn btn-sm btn-danger"><i class="far fa-trash-alt"></i> Xóa</button></td>';
        //     html += '</tr>';
        //     $("#products_views #tbody1").append(html);
        //     console.log(html);
        // }
    }
}

// Xóa sản phẩm trong giỏ hàng
function deleteProductInCart(this_delete) {
    // xoa product trong local theo id
    // lưu local
    let product_id = $(this_delete).attr("product_id");
    let old_data = JSON.parse(localStorage.getItem('card_items')) || [];
    if (product_id) {
        delete old_data[product_id];
    }
    localStorage.setItem('card_items', JSON.stringify(old_data));
}

// Thay đổi số lượng sản phẩm
function changeProductInCart(this_change) {
    // lấy số lượng theo value_input
    let quantity = this_change.val();
    // lấy product_id
    let product_id = this_change.attr("product_id");
    // lấy dũ liệu từ localstorage
    let old_data = JSON.parse(localStorage.getItem('card_items')) || [];
    $.each(old_data, function (index, value) {
        // so sánh value_input và product_id
        if (product_id === value.product_id) {
            // thay số lượng trong localstorage = value_input
            value.quantity = parseInt(quantity);
        }
    });
    // lưu lại localstorage
    localStorage.setItem('card_items', JSON.stringify(old_data));
}

// Thực hiện click xóa sản phẩm
$(document).on('click', '.clear-product-cart', function () {
    deleteProductInCart($(this));
    showProductsInCart();
    showCartCount();
    display_pay();
});

// Thực hiên thay đổi số lượng sản phẩm
$(document).on('change', '.change-quantity-product-cart', function () {
    changeProductInCart($(this));
    showProductsInCart();
});

// Thanh toán
$(".pay").click(function () {
    let note = $('textarea').val();
    localStorage.setItem("note", note);
    console.log(note);
});

// hiển thị sản phẩm trang pay
function showProductInPay() {
    let data = localStorage.getItem("card_items");
    if (data) {
        // chuyển string sang object
        data = JSON.parse(data);
        let sum = 0;
        let html = '';
        // Dùng foreach
        $.each(data, function (index, value) {
            sum += (parseInt(value.price) * parseInt(value.quantity));
            let total = (parseInt(value.price) * parseInt(value.quantity));
            html += '<tr class="text-center">';
            html += '<td style="text-transform: capitalize;">' + value.name + '</td>';
            html += '<td >' + value.quantity + '</td>';
            html += '<td>' + Number(value.price).toLocaleString() + '<span>' + '₫' + '</span>' + '</td>';
            html += '<td>' + Number(total).toLocaleString() + '<span>' + '₫' + '</span>'  + '</td>';
            html += '</tr>';
        });
        html += '<tr>';
        html += '<td colspan="3" style="font-weight: 600; text-align: right" >' + 'Tổng cộng:' + '</td>';
        html += '<td colspan="1" style="text-align: center">' + Number(sum).toLocaleString() + '<span>' + '₫' + '</span>'  + '</td>';
        html += '</tr>';
        // hiển thị ra html
        $("#products_views_pay tbody").html(html);
    }
}

// check validate khi submit form trong trang pay
$(document).on('submit', '#form-pay', function (e) {
    let full_name = $("#full_name").val();
    let name = /[!@#$%\^&*(){}[\]<>?|\-]/;
    if (!full_name) {
        alert('Họ và tên không được để trống');
        e.preventDefault();
    } else {
        if (name.test(full_name)) {
            alert('Họ và tên không viết kí tự đặc biệt và số');
            e.preventDefault();
        } else {
            if (full_name.length < 5) {
                alert('Họ và tên phải có ít nhất 5 kí tự');
                e.preventDefault();
            }
        }
    }

    let number = $("#phone_number").val();
    let intRegex = /^\d+$/;
    let ten_numbers = /^\d{10}$/;
    if (!number) {
        alert('Số điện thoại không được để trống');
        e.preventDefault();
    } else {
        if (!intRegex.test(number)) {
            alert('Số điện thoại phải là số');
            e.preventDefault();
        } else {
            if (!ten_numbers.test(number)) {
                alert('Số điện thoại phải có 10 số');
                e.preventDefault();
            }
        }
    }

    let address = $("#address").val();
    let check_address = /[!@#$%\^&*(){}[\]<>?|\-]/;
    if (!address) {
        alert('Địa chỉ không được để trống');
        e.preventDefault();
    } else {
        if (check_address.test(address)) {
            alert('Địa chỉ không được viết kí tự đặc biệt');
            e.preventDefault();
        } else {
            if (address.length < 10) {
                alert('Địa chỉ phải có ít nhất 10 kí tự');
                e.preventDefault();
            }
        }
    }

    // cách viết 2
    // if (intRegex.test(number) && ten_numbers.test(number)) {
    //     $(this).submit();
    // } else {
    //     alert('no');
    //     e.preventDefault();
    // }
});


// ẩn nút thanh toán khi giỏ hàng không có sản phẩm
function display_pay() {
    let show_cart_count = $('.cart-number-badge').text();
    if (show_cart_count == 0) {
        $('.pay').css("display","none");
    }
}

// check validate khi submit form trong trang vnpay
$(document).on('submit', '#create_form', function (e) {
    let full_name = $("#full_name").val();
    let name = /[!@#$%\^&*(){}[\]<>?|\-]/;
    if (!full_name) {
        alert('Họ và tên không được để trống');
        e.preventDefault();
    } else {
        if (name.test(full_name)) {
            alert('Họ và tên không viết kí tự đặc biệt và số');
            e.preventDefault();
        } else {
            if (full_name.length < 5) {
                alert('Họ và tên phải có ít nhất 5 kí tự');
                e.preventDefault();
            }
        }
    }

    let number = $("#phone_number").val();
    let intRegex = /^\d+$/;
    let ten_numbers = /^\d{10}$/;
    if (!number) {
        alert('Số điện thoại không được để trống');
        e.preventDefault();
    } else {
        if (!intRegex.test(number)) {
            alert('Số điện thoại phải là số');
            e.preventDefault();
        } else {
            if (!ten_numbers.test(number)) {
                alert('Số điện thoại phải có 10 số');
                e.preventDefault();
            }
        }
    }

    let address = $("#address").val();
    let check_address = /[!@#$%\^&*(){}[\]<>?|\-]/;
    if (!address) {
        alert('Địa chỉ không được để trống');
        e.preventDefault();
    } else {
        if (check_address.test(address)) {
            alert('Địa chỉ không được viết kí tự đặc biệt');
            e.preventDefault();
        } else {
            if (address.length < 10) {
                alert('Địa chỉ phải có ít nhất 10 kí tự');
                e.preventDefault();
            }
        }
    }

    let money = $("#amount").val();
    let intRegex_money = /^\d+$/;
    if (!money) {
        alert('Số tiền không được để trống');
        e.preventDefault();
    } else {
        if (!intRegex_money.test(money)) {
            alert('Số tiền phải là số');
            e.preventDefault();
        }
    }

    let order_desc = $("#order_desc").val();
    if (!order_desc) {
        alert('Nội dung không được để trống');
        e.preventDefault();
    }
});

// dừng và tiếp tục post
// $('form').one('submit', function(e) {
//     e.preventDefault();
//     // do your things ...
//
//     // and when you done:
//     $(this).submit();
// });
// regexp

