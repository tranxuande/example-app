<?php

namespace App\Http\Controllers;

use App\Models\admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return Redirect::to('admin')->send();
        }
        return view('login');
    }

    public function login()
    {
        $request = request()->all();
        // username & password
        $username = isset($request['username']) ? $request['username'] : '';
        $password = isset($request['password']) ? $request['password'] : '';
        // check database
        $admin = (new admin)->getAuth($username, $password);
        if ($admin) {
            Auth::login($admin);
            return redirect('admin');
        } else {
            Session::flash('message', 'Tài khoản hoặc mật khẩu không chính xác!');
            return view('login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('login')->send();
    }

    public function admin()
    {
        dd('admin');
    }
}
