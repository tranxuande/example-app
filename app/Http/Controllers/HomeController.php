<?php

namespace App\Http\Controllers;

use App\Models\orders_details;
use App\Models\product_image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
        //Lấy dữ liệu từ products cho section_3
        // Lấy dữ liệu cho phần top
        $products_top = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'publish', 'link', 'created_at', 'update_at')->where('active', 1)->where('publish', 1)->where('percent_discount', '!=', null)->orderBy('percent_discount', 'desc')->take(4)->skip(0)->get();
        $obj['products_top'] = $products_top;
        $product_top_id = [];
        foreach ($products_top as $key => $value) {
            array_push($product_top_id, $value->id);
        }
        // Lấy ảnh sản phẩm theo id trong mảng $product_top_id
        $product_image_db_top = DB::table('product_image')->select('product_id', 'link')->where('active', 1)->whereIn('product_id', $product_top_id)->get();
        //Lấy mảng link ảnh sản phẩm theo id sản phẩm, lấy mảng
        $product_image_top = [];
        foreach ($product_image_db_top as $key => $value) {
            if (!isset($product_image_top[$value->product_id])) {
                $product_image_top[$value->product_id] = [];
            }
            $product_image_top[$value->product_id][] = $value->link;
        }
        $obj['product_image_top'] = $product_image_top;

        // Lấy dữ liệu cho phần bottom
        $products_bottom = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'publish', 'link', 'created_at', 'update_at')->where('active', 1)->where('publish', 1)->where('percent_discount', '!=', null)->orderBy('percent_discount', 'desc')->take(4)->skip(4)->get();
        $obj['products_bottom'] = $products_bottom;

        $product_bottom_id = [];
        foreach ($products_bottom as $key => $value) {
            array_push($product_bottom_id, $value->id);
        }
        // Lấy ảnh sản phẩm theo id trong mảng $product_top_id
        $product_image_db_bottom = DB::table('product_image')->select('product_id', 'link')->where('active', 1)->whereIn('product_id', $product_bottom_id)->get();
        //Lấy mảng link ảnh sản phẩm theo id sản phẩm, lấy mảng
        $product_image_bottom = [];
        foreach ($product_image_db_bottom as $key => $value) {
            if (!isset($product_image_bottom[$value->product_id])) {
                $product_image_bottom[$value->product_id] = [];
            }
            $product_image_bottom[$value->product_id][] = $value->link;
        }
        $obj['product_image_bottom'] = $product_image_bottom;

        //Lấy dữ liệu products cho section_5
        $products_selling = DB::table('products')->select('id', 'name', 'price', 'promotional_price', 'percent_discount', 'link', 'sold')->where('active', 1)->where('publish', 1)->orderBy('sold', 'desc')->take(8)->skip(0)->get();
        $obj['products_selling'] = $products_selling;
        //Lấy product id từ $products_selling
        $product_selling_id = [];
        foreach ($products_selling as $key => $value) {
            array_push($product_selling_id, $value->id);
        }
        // Lấy ảnh sản phẩm theo id trong mảng $product_selling_id
        $product_image_db_selling = DB::table('product_image')->select('product_id', 'link')->where('active', 1)->whereIn('product_id', $product_selling_id)->get();
        //Lấy mảng link ảnh sản phẩm theo id sản phẩm, lấy mảng
        $product_image_selling = [];
        foreach ($product_image_db_selling as $key => $value) {
            if (!isset($product_image_selling[$value->product_id])) {
                $product_image_selling[$value->product_id] = [];
            }
            $product_image_selling[$value->product_id][] = $value->link;
        }
        $obj['product_image_selling'] = $product_image_selling;

        // Lấy dữ liệu products cho section_6
        $products_new = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'publish', 'link', 'created_at', 'update_at')->where('active', 1)->where('publish', 1)->orderBy('created_at', 'desc')->take(4)->skip(0)->get();
        $obj['products_new'] = $products_new;
        //Lấy product_id từ $products_new
        $product_new_id = [];
        foreach ($products_new as $key => $value) {
            array_push($product_new_id, $value->id);
        }
        // Lấy ảnh sản phẩm theo id trong mảng $product_new_id
        $product_image_db_new = DB::table('product_image')->select('product_id', 'link')->where('active', 1)->whereIn('product_id', $product_new_id)->get();
        $obj['product_image_db_new'] = $product_image_db_new;
        // Lấy mảng link ảnh sản phẩm theo id sản phẩm, lấy mảng,
        $product_image_new = [];
        foreach ($product_image_db_new as $key => $value) {
            if (!isset($product_image_new[$value->product_id])) {
                $product_image_new[$value->product_id] = [];
            }
            $product_image_new[$value->product_id][] = $value->link;
        }
        $obj['product_image_new'] = $product_image_new;
        // Lấy dữ liệu cho section1

        $slide_image = DB::table('slide_image')->select('id', 'name', 'link_image', 'product_id', 'product_link')->where('active', 1)->where('publish', 1)->orderBy('created_at', 'desc')->get();
        $obj['slide_image'] = $slide_image;


        return view('home')->with($obj);

    }

    // chi tiết sản phẩm
    public function product_detail($url)
    {
        // Lấy dữ liệu 1 sản phẩm bằng link của sản phẩm
        $products = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'description', 'content', 'publish', 'link', 'quantily', 'sold', 'created_at', 'update_at')->where('active', 1)->where('publish', 1)->where('link', $url)->first();
        $obj['products'] = $products;
        // Lấy mảng tất cả link ảnh của 1 sản phẩm
        $product_image_db_detail = DB::table('product_image')->select('link')->where('active', 1)->where('product_id', $products->id)->pluck('link')->toArray();
        $obj['product_image_db_detail'] = $product_image_db_detail;

        // Lấy dữ liệu từ bảng products cho phần sản phẩm liên quan
        $products_related = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'publish', 'link', 'created_at', 'update_at')->where('active', 1)->where('publish', 1)->where('category_id', $products->category_id)->where('id', '!=', $products->id)->orderBy('id', 'desc')->take(4)->skip(0)->get();
        $obj['products_related'] = $products_related;
        //Lấy mảng id
        $product_related_id = [];
        foreach ($products_related as $key => $value) {
            // array_push($product_related_id, $value->id);
            $product_related_id[] = $value->id;
        }
        // Lấy ảnh sản phẩm theo id trong mảng $product_top_id
        $product_image_db_related = DB::table('product_image')->select('product_id', 'link')->where('active', 1)->whereIn('product_id', $product_related_id)->get();

        //Lấy mảng link ảnh sản phẩm theo id sản phẩm, lấy mảng
        $product_image_related = [];
        foreach ($product_image_db_related as $key => $value) {
            if (!isset($product_image_related[$value->product_id])) {
                $product_image_related[$value->product_id] = [];
            }
            $product_image_related[$value->product_id][] = $value->link;
        }
        $obj['product_image_related'] = $product_image_related;
        return view('product_detail')->with($obj);
    }

    // danh mục sản phẩm danh sách sản phẩm
    public function product_category($url)
    {
        // Lấy dữ liệu danh mục sản phẩm bằng link của danh mục
        $category = DB::table('category')->select('id', 'parent_id')->where('link_category', $url)->first();
        $obj['category'] = $category;
        // Check $category->parent_id
        if ($category->parent_id != null) {
            // Lấy dữ liệu cho danh mục con
            $category_id = [$category->id];
        } else {
            // Lấy dữ liệu bằng id với điều kiện parent_id == $category->id, lấy dữ liệu cho danh mục cha
            $category_child = DB::table('category')->select('id')->where('parent_id', $category->id)->pluck('id')->toArray();
            $category_id = $category_child;
        }
        $obj['category_id'] = $category_id;
        //Lấy dữ liệu từ bảng products cho trang product_list
        $products = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'link', 'created_at', 'sold')->where('active', 1)->where('publish', 1)->whereIn('category_id', $category_id);
        $request = request()->all();
        // tìm kiếm theo danh mục
        $products = $this->search_filter($products, $request);
        // hiển thị
        $sold = isset($request['sold']) ? isset($request['sold']) : '';
        if ($sold != '') {
            // hiển thị sản phẩm theo số lượng đã bán
            $products = $products->orderBy('sold', 'desc')->paginate(6);
        } else {
            $promotion = isset($request['promotion']) ? isset($request['promotion']) : '';
            if ($promotion != '') {
                // hiển thị sản phẩm theo phần trăm giảm giá
                $products = $products->where('percent_discount', '!=', null)->orderBy('percent_discount', 'desc')->paginate(6);
            } else {
                //Phân trang và hiển thị sản phẩm theo id
                $products = $products->orderBy('id', 'desc')->paginate(6);
            }
        }
        $obj['products'] = $products;
        //Lấy id
        $products_id = [];
        foreach ($products as $key => $value) {
            $products_id[] = $value->id;

        }
        // Lấy ảnh sản phẩm theo id trong mảng $product_id
        $product_image_db_list = DB::table('product_image')->select('product_id', 'link')->where('active', 1)->whereIn('product_id', $products_id)->get();
        //Lấy mảng link ảnh sản phẩm theo id sản phẩm, lấy mảng
        $product_image_list = [];
        foreach ($product_image_db_list as $key => $value) {
            if (!isset($product_image_list[$value->product_id])) {
                $product_image_list[$value->product_id] = [];
            }
            $product_image_list[$value->product_id][] = $value->link;
        }
        $obj['product_image_list'] = $product_image_list;

        return view('product_list')->with($obj);
    }

    // tìm kiếm
    public function product_search()
    {
        $products = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'link')->where('active', 1)->where('publish', 1);
        $request = request()->all();
        $search = isset($request['search']) ? $request['search'] : '';
        if ($search != '') {
            $products = $products->where('name', 'like', '%' . $search . '%');
        }

        // tìm kiếm tất cả Search all
        $products = $this->search_filter($products, $request);
        // Hiển thị
        $sold = isset($request['sold']) ? isset($request['sold']) : '';
        if ($sold != '') {
            // hiển thị sản phẩm theo số lượng đã bán
            $products = $products->orderBy('sold', 'desc')->paginate(6);
        } else {
            $promotion = isset($request['promotion']) ? isset($request['promotion']) : '';
            if ($promotion != '') {
                // hiển thị sản phẩm theo phần trăm giảm giá
                $products = $products->where('percent_discount', '!=', null)->orderBy('percent_discount', 'desc')->paginate(6);
            } else {
                //Phân trang và hiển thị sản phẩm theo id
                $products = $products->orderBy('id', 'desc')->paginate(6);
            }
        }
        $obj['products'] = $products;
        //Lấy id
        $products_id = [];
        foreach ($products as $key => $value) {
            $products_id[] = $value->id;
        }
        $product_image_db_list = DB::table('product_image')->select('product_id', 'link')->where('active', 1)->whereIn('product_id', $products_id)->get();
        //Lấy mảng link ảnh sản phẩm theo id sản phẩm, lấy mảng
        $product_image_list = [];
        foreach ($product_image_db_list as $key => $value) {
            if (!isset($product_image_list[$value->product_id])) {
                $product_image_list[$value->product_id] = [];
            }
            $product_image_list[$value->product_id][] = $value->link;
        }
        $obj['product_image_list'] = $product_image_list;

        return view('product_list')->with($obj);
    }

    //Bộ lọc tìm kiếm search_filter
    private function search_filter($products, $request)
    {
        $name = isset($request['name']) ? $request['name'] : '';
        if ($name != '') {
            $products = $products->where('name', 'like', '%' . $name . '%');
        }
        // Price_from
        $price_from = isset($request['price_from']) ? $request['price_from'] : '';
        if ($price_from != '') {
            $products = $products->where('price', '>=', $price_from);
        }
        // Price_to
        $price_to = isset($request['price_to']) ? $request['price_to'] : '';
        if ($price_to != '') {
            $products = $products->where('price', '<=', $price_to);
        }
        return $products;
    }

    // giỏ hàng
    public function display_cart()
    {
        return view('cart.home_cart');
    }

    // thanh toán
    public function pay()
    {
        return view('pay.home_pay');
    }

    //
    public function payment_methods()
    {
        return view('payment_methods.home_payment_methods');
    }

    //
    public function store_pay()
    {
        session()->flashInput(request()->input());
        $request = request()->all();
        $messages = $this->validate_pay();
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            return view('pay.home_pay');
        } else {
            // Insert vào cùng bảng
            $request['note'] = $this->note();
            $orders = (new \App\Models\orders())->create($request);
            $this->orders_details($orders->id);
            Session::flash('message', 'Đã gửu hóa đơn thành công');
            return redirect('/payment-methods');
        }
    }

    // Insert bảng orders_details
    private function orders_details($orders_id)
    {
        $order = request()->input('card_items');
        $decoded = json_decode($order);
        if ($order) {
            $orders = [];
            $products = DB::table('products')->select('id', 'price', 'promotional_price')->where('active', 1)->where('publish', 1)->get();
            foreach ($decoded as $key => $item) {
                foreach ($products as $key1 => $value) {
                    if (isset($item->product_id) && $item->product_id == $value->id) {
                        if ($value->promotional_price) {
                            $orders_details = [
                                'orders_id' => $orders_id,
                                'product_id' => $item->product_id,
                                'money' => $value->promotional_price,
                                'quantity' => $item->quantity,
                                'total_money' => $value->promotional_price * $item->quantity
                            ];
                            $orders[] = $orders_details;
                        } else {
                            if ($value->price) {
                                $orders_details = [
                                    'orders_id' => $orders_id,
                                    'product_id' => $item->product_id,
                                    'money' => $value->price,
                                    'quantity' => $item->quantity,
                                    'total_money' => $value->price * $item->quantity
                                ];
                                $orders[] = $orders_details;
                            }
                        }
                    }
                }
            }
            if ($orders) {
                orders_details::insert($orders);
            }
        }
    }

    // Insert note vào bảng orders
    private function note()
    {
        $note = request()->input('note');
        return $note;

    }

    private function validate_pay()
    {
        $request = request()->all();
        $messages = [];
        //Check name
        $name = isset($request['full_name']) ? $request['full_name'] : '';
        if ($name == '') {
            $messages[] = 'Họ và tên là bắt buộc';
        } else {
            if (preg_match('/[\'^£$!%&*()}{@#~?><>,|=_+¬\d-]/', $name)) {
                $messages[] = 'Họ và tên không được viết kí tự đặc biệt và số';
            } else {
                if (strlen($name) <= 5) {
                    $messages[] = 'Họ và tên phải có ít nhất 5 kí tự';
                }
            }
        }
        // Check phone_number

        $phone_number = isset($request['phone_number']) ? $request['phone_number'] : '';
        if ($phone_number == '') {
            $messages[] = 'Số điện thoại là bắt buộc';
        } else {
            if (is_numeric($phone_number) == false) {
                $messages[] = 'Số điện thoại phải là số';
            } else {
                if (strlen($phone_number) != 10) {
                    $messages[] = 'Số điện thoại phải có 10 số';
                }
            }

        }
        // Check address
        $address = isset($request['address']) ? $request['address'] : '';
        if ($address == '' || mb_strlen($address) > 250) {
            if ($address == '') {
                $messages[] = 'Địa chỉ là bắt buộc.';
            } else {
                $messages[] = 'Địa chỉ không được lớn hơn 250 ký tự.';

            }
        } else {
            if (preg_match('/[!@#$%\^&*(){}[\]<>?|\-]/', $address)) {
                $messages[] = 'Địa chỉ không được viết kí tự đặc biệt';
            } else {
                if (strlen($address) <= 10) {
                    $messages[] = 'Địa chỉ phải có ít nhất 10 kí tự';
                }
            }
        }

        return $messages;
    }

    // giới thiệu
    public function introduce()
    {
        return view('introduce.home_introduce');
    }

    // tin tức
    public function news()
    {
        $news = DB::table('news')->select('id', 'title', 'link_news', 'link_image', 'update_at')->where('active', 1)->where('publish', 1);
        $news = $news->orderBy('id', 'desc')->paginate(9);
        $obj['news'] = $news;
        return view('news.home_news')->with($obj);
    }

    // chi tiết tin tức
    public function details_news($url)
    {
        $details_news = DB::table('news')->select('id', 'title', 'link_news', 'link_image', 'content', 'update_at')->where('active', 1)->where('publish', 1)->where('link_news', $url)->first();
        $news = DB::table('news')->select('id', 'title', 'link_news', 'link_image', 'update_at')->where('active', 1)->where('publish', 1)->where('id', '!=', $details_news->id)->orderBy('update_at', 'desc')->take(5)->skip(0)->get();
        $obj['details_news'] = $details_news;
        $obj['news'] = $news;
        return view('news.details_news')->with($obj);
    }

    // liên hệ
    public function contact()
    {
        return view('contact.home_contact');
    }

    // quy định đổi trả
    public function return_policy()
    {
        return view('return_policy.home_return_policy');
    }

    public function vn_pay()
    {
        $startTime = date("YmdHis");
        $obj = [
            'expire' => date('YmdHis', strtotime('+15 minutes', strtotime($startTime))),
        ];
        return view('pay/vn_pay_create')->with($obj);
    }

    public function vnpay_create_payment()
    {
        $startTime = date("YmdHis");
        $obj = [
            'expire' => date('YmdHis', strtotime('+15 minutes', strtotime($startTime))),
        ];
        session()->flashInput(request()->input());
        $request = request()->all();
        $messages = $this->validate_vnpay();
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            return view('pay/vn_pay_create')->with($obj);
        } else {
            // Insert vào cùng bảng
            $orders = (new \App\Models\orders())->create($request);
            $this->orders_details($orders->id);
        }

        $vnp_TmnCode = "M0VA552L"; //Website ID in VNPAY System
        $vnp_HashSecret = "XMOCUXXHQNJINHSTYUOGYFEUQBCYHQQM"; //Secret key
        $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = "http://localhost:8000/vnpay-return";
        $vnp_apiUrl = "http://sandbox.vnpayment.vn/merchant_webapi/merchant.html";

        $vnp_TxnRef = $_POST['order_id']; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = $_POST['order_desc'];
        $vnp_OrderType = $_POST['order_type'];
        $vnp_Amount = $_POST['amount'] * 100;
        $vnp_Locale = $_POST['language'];
        $vnp_BankCode = $_POST['bank_code'];
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];
        //Add Params of 2.0.1 Version
        $vnp_ExpireDate = $_POST['txtexpire'];
        //Billing
        $vnp_Bill_Mobile = $_POST['phone_number'];
        $vnp_Bill_Address = $_POST['address'];
        $vnp_Bill_fullName = ($_POST['full_name']);
//        if (isset($fullName) && trim($fullName) != '') {
//            $name = explode(' ', $fullName);
//            $vnp_Bill_FirstName = array_shift($name);
//            $vnp_Bill_LastName = array_pop($name);
//        }
//        $vnp_Bill_Address = $_POST['txt_inv_addr1'];
//        $vnp_Bill_City = $_POST['txt_bill_city'];
//        $vnp_Bill_Country = $_POST['txt_bill_country'];
//        $vnp_Bill_State = $_POST['txt_bill_state'];
//        // Invoice
//        $vnp_Inv_Phone = $_POST['txt_inv_mobile'];
//        $vnp_Inv_Email = $_POST['txt_inv_email'];
//        $vnp_Inv_Customer = $_POST['txt_inv_customer'];
//        $vnp_Inv_Address = $_POST['txt_inv_addr1'];
//        $vnp_Inv_Company = $_POST['txt_inv_company'];
//        $vnp_Inv_Taxcode = $_POST['txt_inv_taxcode'];
//        $vnp_Inv_Type = $_POST['cbo_inv_type'];
        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
            "vnp_ExpireDate" => $vnp_ExpireDate,
            "vnp_Bill_Mobile" => $vnp_Bill_Mobile,
            "vnp_Bill_Address" => $vnp_Bill_Address,
            "vnp_Bill_fullName" => $vnp_Bill_fullName,
//            "vnp_Bill_FirstName" => $vnp_Bill_FirstName,
//            "vnp_Bill_LastName" => $vnp_Bill_LastName,
//            "vnp_Bill_Address" => $vnp_Bill_Address,
//            "vnp_Bill_City" => $vnp_Bill_City,
//            "vnp_Bill_Country" => $vnp_Bill_Country,
//            "vnp_Inv_Phone" => $vnp_Inv_Phone,
//            "vnp_Inv_Email" => $vnp_Inv_Email,
//            "vnp_Inv_Customer" => $vnp_Inv_Customer,
//            "vnp_Inv_Address" => $vnp_Inv_Address,
//            "vnp_Inv_Company" => $vnp_Inv_Company,
//            "vnp_Inv_Taxcode" => $vnp_Inv_Taxcode,
//            "vnp_Inv_Type" => $vnp_Inv_Type
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        if (isset($vnp_Bill_State) && $vnp_Bill_State != "") {
            $inputData['vnp_Bill_State'] = $vnp_Bill_State;
        }
//        dd($inputData);
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash = hash_hmac('sha512', $hashdata, $vnp_HashSecret);//
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array('code' => '00'
        , 'message' => 'success'
        , 'data' => $vnp_Url);
        if (isset($_POST['redirect'])) {
            header('Location: ' . $vnp_Url);
            die();
        } else {
            echo json_encode($returnData);
        }
        dd(1);
    }

    public function vnpay_return() {
        $vnp_SecureHash = $_GET['vnp_SecureHash'];
        $vnp_HashSecret = "XMOCUXXHQNJINHSTYUOGYFEUQBCYHQQM";
        $inputData = array();
        foreach ($_GET as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }

        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashData = $hashData . urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
        }
        $secureHash = hash_hmac('sha512', $hashData, $vnp_HashSecret);
        $obj = [
            'secureHash' => $secureHash,
            'vnp_SecureHash' => $vnp_SecureHash,
        ];
        return view('pay/vn_pay_return')->with($obj);
    }

    private function validate_vnpay() {
        $request = request()->all();
        $messages = [];
        //Check name
        $name = isset($request['full_name']) ? $request['full_name'] : '';
        if ($name == '') {
            $messages[] = 'Họ và tên là bắt buộc';
        } else {
            if (preg_match('/[\'^£$!%&*()}{@#~?><>,|=_+¬\d-]/', $name)) {
                $messages[] = 'Họ và tên không được viết kí tự đặc biệt và số';
            } else {
                if (strlen($name) <= 5) {
                    $messages[] = 'Họ và tên phải có ít nhất 5 kí tự';
                }
            }
        }
        // Check phone_number
        $phone_number = isset($request['phone_number']) ? $request['phone_number'] : '';
        if ($phone_number == '') {
            $messages[] = 'Số điện thoại là bắt buộc';
        } else {
            if (is_numeric($phone_number) == false) {
                $messages[] = 'Số điện thoại phải là số';
            } else {
                if (strlen($phone_number) != 10) {
                    $messages[] = 'Số điện thoại phải có 10 số';
                }
            }

        }
        // Check address
        $address = isset($request['address']) ? $request['address'] : '';
        if ($address == '' || mb_strlen($address) > 250) {
            if ($address == '') {
                $messages[] = 'Địa chỉ là bắt buộc.';
            } else {
                $messages[] = 'Địa chỉ không được lớn hơn 250 ký tự.';

            }
        } else {
            if (preg_match('/[!@#$%\^&*(){}[\]<>?|\-]/', $address)) {
                $messages[] = 'Địa chỉ không được viết kí tự đặc biệt';
            } else {
                if (strlen($address) <= 10) {
                    $messages[] = 'Địa chỉ phải có ít nhất 10 kí tự';
                }
            }
        }
        // check money
        $money = isset($request['amount']) ? $request['amount'] : '';
        if ($money == '') {
            $messages[] = 'Số tiền không được để trống';
        } else {
            if (is_numeric($money) == false) {
                $messages[] = 'Số tiền phải là số';
            }
        }
        // check content
        $content = isset($request['order_desc']) ? $request['order_desc'] : '';
        if ($content == '') {
            $messages[] = 'Nội dung hóa đơn không được để trống';
        }

        return $messages;
    }
}
