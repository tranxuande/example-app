<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\product_image;
use App\Models\products;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends AdminController
{
    public function index()
    {
        $obj = $this->data_before();
        // Lấy dữ liệu sản phẩm từ db
        $products = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'quantily', 'publish', 'sold', 'created_at', 'update_at')->where('active', 1);

        $request = request()->all();

        // Tìm kiếm theo từng trường

        // Name
        $name = isset($request['name']) ? $request['name'] : '';
        if ($name != '') {
            $products = $products->where('name', 'like', '%' . $name . '%');
        }
        // Category
        $category = isset($request['category']) ? $request['category'] : '';
        if ($category != '') {
            $products = $products->where('category_id', $category);
        }
        // Publish
        $publish = isset($request['publish']) ? $request['publish'] : '';
        if ($publish != '') {
            $products = $products->where('publish', $publish);
        }
        // Price_from
        $price_from = isset($request['price_from']) ? $request['price_from'] : '';
        if ($price_from != '') {
            $products = $products->where('price', '>=', $price_from);
        }
        // Price_to
        $price_to = isset($request['price_to']) ? $request['price_to'] : '';
        if ($price_to != '') {
            $products = $products->where('price', '<=', $price_to);
        }
        // Date_from
        $date_from = isset($request['date_from']) ? $request['date_from'] : '';
        if ($date_from != '') {
            $products = $products->where('created_at', '>=', $date_from);
        }
        // Date_to
        $date_to = isset($request['date_to']) ? $request['date_to'] . ' 23:59:59' : '';
        if ($date_to != '') {
            $products = $products->where('created_at', '<=', $date_to);
        }
        // Phân trang
        $products = $products->orderBy('id', 'desc')->paginate(10);
        $obj['products'] = $products;
        // Lấy product id từ $products
        $product_id = [];
        foreach ($products as $key => $value) {
            array_push($product_id, $value->id);
        }
        // Lấy ảnh sản phẩm từ database có active = 1 và product_id có trong $product_id, lấy chuỗi, lấy một ảnh, lấy link 1 ảnh theo id của cản phẩm
        $product_image = DB::table('product_image')->select('product_id', 'link')->where('active', 1)->whereIn('product_id', $product_id)->groupBy('product_id')->pluck('link', 'product_id')->toArray();
        $obj['product_image'] = $product_image;
        // thống kê
        $total_products = DB::table('products')->select( DB::raw('count(products.id) as total_products'))->where('active', 1)->first();
        $products_promotion = DB::table('products')->select( DB::raw('count(products.percent_discount) as products_promotion'))->where('active', 1)->first();
        $products_not_displayed = DB::table('products')->select( DB::raw('count(products.id) as products_not_displayed'))->where('active', 1)->where('publish', 0)->first();
        $obj['total_products'] = $total_products;
        $obj['products_promotion'] = $products_promotion;
        $obj['products_not_displayed'] = $products_not_displayed;
        return view('admin_product.product')->with($obj);
    }

    public function create()
    {
        $obj = $this->data_before();
        return view('admin_product.product_form')->with($obj);
    }
    //
    public function store()
    {
        $obj = $this->data_before();
        session()->flashInput(request()->input());
        $request = request()->all();
        $messages = $this->validate_product();
        $image = isset($request['photos']) ? $request['photos'] : '';
        if ($image == '') {
            $messages[] = 'Hình ảnh không được để trống';
        }
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            return view('admin_product.product_form')->with($obj);
        } else {
            // Insert vào khác bảng $this->product_image($record['id']);
            $record = (new \App\Models\products)->create($request);
            $this->product_image($record['id']);
            Session::flash('message', 'Tạo sản phẩm thành công!');
            return redirect('/product');
        }
    }

    public function edit($id)
    {
        $obj = $this->data_before();
        $product = DB::table('products')->select('id', 'name', 'category_id', 'price', 'promotional_price', 'description', 'quantily', 'content', 'publish')->where('id', $id)->where('active', 1)->first();
        if (!$product) {
            abort(404);
        }
        $product_image = DB::table('product_image')->select('link')->where('product_id', $id)->where('active', 1)->get();
        $obj['product'] = $product;
        $obj['product_image'] = $product_image;
        return view('admin_product.product_form')->with($obj);
    }

    public function update()
    {
        $obj = $this->data_before();
        session()->flashInput(request()->input());
        $messages = $this->validate_product();
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            $obj['product'] = (object)(request()->all());
            return view('admin_product.product_form')->with($obj);
        } else {
            $request = request()->all();
            products::find($request['id'])->update($request);
            $this->product_image($request['id']);
            Session::flash('message', 'Cập nhật sản phẩm thành công!');
            return redirect('/product');
        }
    }

    public function destroy($id)
    {
        products::find($id)->update(['active' => 0]);
        Session::flash('message', 'Xóa sản phẩm thành công');
        return redirect('/product');
    }

    //Yêu cầu người quản trị
    private function validate_product()
    {
        $request = request()->all();
//        $validator = Validator::make(request()->all(),
//            [
//                'name' => 'required|string|max:250',
//                'category_id' => 'required|integer',
//            ],
//            [
//                'name.required' => 'Trường tên là bắt buộc.',
//            ]);
//        if ($validator->fails()) {
//            dd($validator->messages());
//        }
        $messages = [];
        // Check name
        $name = isset($request['name']) ? $request['name'] : '';
        if ($name == '' || mb_strlen($name) > 250) {
            if ($name == '') {
                $messages[] = 'Trường tên là bắt buộc.';
            } else {
                $messages[] = 'Trường tên không được lớn hơn 250 ký tự.';
            }
        }
        // Check category
        $category = isset($request['category_id']) ? $request['category_id'] : '';
        if ($category == '' || !is_int((int)$category)) {
            if ($category == '') {
                $messages[] = 'Phân loại không được để trống.';
            } else {
                $messages[] = 'Phân loại định dạng dữ liệu bị sai.';
            }
        }
        // Check price
        $price = isset($request['price']) ? $request['price'] : '';
        if ($price == '') {
            $messages[] = 'Giá tiền không được để trống';
        } else {
            if (is_numeric($price) == false) {
                $messages[] = 'Giá tiền phải là số';
            }
        }
        // Check promotional_price
        $promotional_price = isset($request['promotional_price']) ? $request['promotional_price'] : '';
        if ($promotional_price != '' && !is_numeric($promotional_price)) {
            $messages[] = 'Giá tiền phải là số';
        }
        // Check promotional_price
        if ($promotional_price != '' && $price != '') {
            if ($promotional_price > $price) {
                $messages[] = 'Giá khuyến mại phải nhỏ hơn giá gốc';
            }

        }
        // Check quantily
        $quantily = isset($request['quantily']) ? $request['quantily'] : '';
        if ($quantily == '') {
            $messages[] = 'Số lượng không được để trống';
        } else {
            if (is_numeric($quantily) == false) {
                $messages[] = 'Số lượng phải là số';
            }
        }
        // Check publish
        $publish = isset($request['publish']) ? $request['publish'] : '';
        if ($publish != 0 && $publish != 1) {
            $messages[] = 'Dữ liệu không hợp lệ';
        }
        // Check description
        $description = isset($request['description']) ? $request['description'] : '';
        if ($description == '') {
            $messages[] = 'Mô tả không được để trống';
        } else {
            if (mb_strlen($description) > 500) {
                $messages[] = 'Mô tả không được vượt qua 500 ký tự';
            }
        }
        // Check content
        $content = isset($request['content']) ? $request['content'] : '';
        if ($content == '') {
            $messages[] = 'Nội dung không được để trống';
        }
        return $messages;
    }
    //Xử lý ảnh sản phẩm, lưu nhiều ảnh
    private function product_image($id)
    {
        $images = request()->file('photos');
        if ($images) {
            $img_data = [];
            foreach ($images as $key => $value) {
                $filename = time() . '_' . $key . '_' . $value->getClientOriginalName();
                $path = '/images/products/' . $filename;
//                $new_img = Image::make($value->getRealPath())->fit(360, 462);
                $new_img = Image::make($value->getRealPath());
                // save file with medium quality
                $new_img->save(public_path() . $path, 80);

                $img = [
                    'product_id' => $id,
                    'link' => $path,
                    'active' => 1,
                ];
                $img_data[] = $img;
            }
            if ($img_data) {
                product_image::insert($img_data);
            }
        }
    }
    //Lấy dữ liệu ban đầu
    private function data_before()
    {
        //
        $category = DB::table('category')->select('id', 'name')->where('parent_id', '!=', null)->pluck('name', 'id')->toArray();
        return [
            'category' => $category
        ];
    }
}
