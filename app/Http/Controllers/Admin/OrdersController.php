<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;
use App\Models\news;
use App\Models\orders;
use App\Models\orders_details;
use App\Models\products;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use function Symfony\Component\String\s;

class OrdersController extends AdminController
{
    public function index()
    {
        $obj = $this->data_before();
        $orders = DB::table('orders')->select('id', 'code_orders', 'full_name', 'phone_number', 'address', 'note', 'order_status_id', 'cancellation_reason', 'active', 'created_at', 'update_at')->where('active', 1);
        $request = request()->all();
        // code_orders
        $code_orders = isset($request['code_orders']) ? $request['code_orders'] : '';
        if ($code_orders != '') {
            $orders = $orders->where('code_orders', 'like', '%' . $code_orders . '%');
        }
        // full_name
        $full_name = isset($request['full_name']) ? $request['full_name'] : '';
        if ($full_name != '') {
            $orders = $orders->where('full_name', $full_name);
        }
        // phone_number
        $phone_number = isset($request['phone_number']) ? $request['phone_number'] : '';
        if ($phone_number != '') {
            $orders = $orders->where('phone_number', $phone_number);
        }
        // address
        $address = isset($request['address']) ? $request['address'] : '';
        if ($address != '') {
            $orders = $orders->where('address', $address);
        }
        // order_status_id
        $order_status_id = isset($request['order_status_id']) ? $request['order_status_id'] : '';
        if ($order_status_id != '') {
            $orders = $orders->where('order_status_id', $order_status_id);
        }
        // Date_from
        $date_from = isset($request['date_from']) ? $request['date_from'] : '';
        if ($date_from != '') {
            $orders = $orders->where('created_at', '>=', $date_from);
        }
        // Date_to
        $date_to = isset($request['date_to']) ? $request['date_to'] . ' 23:59:59' : '';
        if ($date_to != '') {
            $orders = $orders->where('created_at', '<=', $date_to);
        }
        $orders = $orders->orderBy('id', 'desc')->paginate(10);
        $obj['orders'] = $orders;
        // thống kê
        $total_orders = DB::table('orders')->select( DB::raw('count(orders.id) as total_orders'))->where('active', 1)->first();
        $orders_completed_order = DB::table('orders')->select( DB::raw('count(orders.id) as orders_completed_order'))->where('active', 1)->where('order_status_id', 5)->first();
        $total_order_amount = DB::table('orders_details')
            ->select( DB::raw('SUM(orders_details.total_money) as total_order_amount'))
            ->join('orders', 'orders_details.orders_id', 'orders.id')
            ->where('orders.order_status_id', 5)
            ->where('orders_details.active', 1)
            ->where('orders.active', 1)
            ->first();
        $obj['total_orders'] = $total_orders;
        $obj['orders_completed_order'] = $orders_completed_order;
        $obj['total_order_amount'] = $total_order_amount;
        return view('admin_order.order')->with($obj);
    }

    public function create()
    {
        $obj = $this->data_before();
        return view('admin_order.order_form')->with($obj);
    }

    public function store()
    {
//        $request = request()->all();
//
//
//        $request['order_status_id'] = $this->status();
//
//        orders::find($request['id'])->update($request);
//        Session::flash('message', 'Cập nhật sản phẩm thành công!');
//        return redirect('/orders');

        $obj = $this->data_before();
        session()->flashInput(request()->input());
        $request = request()->all();

        $messages = $this->validate_oder();
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            return view('admin_order.order_form')->with($obj);
        } else {
            // Insert vào cùng bảng
            (new \App\Models\orders)->create($request);
            Session::flash('message', 'Tạo sản phẩm thành công!');
            return redirect('/orders');
        }

    }

    public function edit($id)
    {
        $obj = $this->data_before();
        $orders = DB::table('orders')->select('id', 'full_name', 'phone_number', 'address', 'note', 'order_status_id', 'cancellation_reason', 'created_at', 'update_at')->where('id', $id)->where('active', 1)->first();
        if (!$orders) {
            abort(404);
        }
        $orders_details = DB::table('orders_details')->select('id', 'product_id', 'quantity', 'money', 'total_money')->where('orders_id', $id)->where('active', 1)->get();
        $total = 0;
        foreach ($orders_details as $key => $value) {
            $total += $value->quantity * $value->money;
        }
        $obj['total'] = $total;
        $obj['orders'] = $orders;
        $obj['orders_details'] = $orders_details;
        return view('admin_order.order_form')->with($obj);
    }

    public function update()
    {
        session()->flashInput(request()->input());
        $messages = $this->validate_oder();
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            $obj['orders'] = (object)(request()->all());
            return view('admin_order.order_form')->with($obj);
        } else {
            $request = request()->all();
            $order = orders::find($request['id']);
            $old_status = $order->order_status_id;
            $order->update($request);

            $status = $request['order_status_id'];
            if ($old_status != $status && $status == 5) {
                $order_detail_products = DB::table('orders_details')
                    ->select('product_id')
                    ->where('orders_id', $request['id'])
                    ->where('active', 1)
                    ->groupBy('product_id')
                    ->get()
                    ->pluck('product_id')
                    ->toArray();
                $total = DB::table('orders_details')
                    ->select(
                        'orders_details.product_id',
                        DB::raw('SUM(orders_details.quantity) as quantity')
                    )
                    ->join('orders', 'orders_details.orders_id', 'orders.id')
                    ->where('orders.order_status_id', 5)
                    ->whereIn('orders_details.product_id', $order_detail_products)
                    ->where('orders_details.active', 1)
                    ->groupBy('orders_details.product_id')
                    ->orderBy('orders_details.product_id', 'asc')
                    ->get()->toArray();
                $orders_id = $request['id'];
                $total_2 = DB::table('orders_details')
                    ->select(
                        'orders_details.product_id', 'orders_details.orders_id',
                        DB::raw('orders_details.quantity as quantity')
                    )
                    ->join('orders', 'orders_details.orders_id', 'orders.id')
                    ->where('orders_details.orders_id', $orders_id)
                    ->whereIn('orders_details.product_id', $order_detail_products)
                    ->where('orders_details.active', 1)
                    ->groupBy('orders_details.product_id')
                    ->orderBy('orders_details.product_id', 'asc')
                    ->get()->toArray();
                foreach ($total as $key => $value) {
//                    foreach ($total_2 as $item) {
                        $product = products::find($value->product_id);
//                        $con_lai = $product->quantily - $total_2[$key]->quantity;
                        $product->update([
//                            'quantily' => $con_lai,
                            'sold' => $value->quantity
                        ]);
                }
                foreach ($total_2 as $key => $value) {
//                    foreach ($total_2 as $item) {
                    $product = products::find($value->product_id);
                    $con_lai = $product->quantily - $value->quantity;
                    $product->update([
                        'quantily' => $con_lai,
//                        'sold' => $value->quantity
                    ]);
                }
                // cách tối ưu
//                foreach ($total as $key => $value) {
////                    foreach ($total_2 as $item) {
//                    $product = products::find($value->product_id);
//                    $con_lai = $product->quantily - $total_2[$key]->quantity;
//                    $product->update([
//                        'quantily' => $con_lai,
//                        'sold' => $value->quantity
//                    ]);
//                }
            }
            Session::flash('message', 'Cập nhật sản phẩm thành công!');
            return redirect('/orders');
        }
    }

    public function destroy($id)
    {
        orders::find($id)->update(['active' => 0]);
        Session::flash('message', 'Xóa sản phẩm thành công');
        return redirect('/orders');
    }
//    public function update_status() {
//
//        $request = request()->all();
//        $request['order_status_id'] = $this->status();
//        orders::find($request['id'])->update($request);
//        Session::flash('message', 'Cập nhật sản phẩm thành công!');
//        return redirect('/orders');
//
//
//    }
    private function status()
    {
        $order_status_id = request()->input('order_status_id');
        return $order_status_id;
    }

    private function validate_oder()
    {
        $request = request()->all();
        $messages = [];
        //Check name
        $name = isset($request['full_name']) ? $request['full_name'] : '';
        if ($name == '') {
            $messages[] = 'Trường tên là bắt buộc';
        }
        // Check phone_number

        $phone_number = isset($request['phone_number']) ? $request['phone_number'] : '';
        if ($phone_number == '') {
            $messages[] = 'Số điện thoại là bắt buộc';
        } else {
            if (is_numeric($phone_number) == false) {
                $messages[] = 'Số điện thoại phải là số';
            } else {
                if (strlen($phone_number) != 10) {
                    $messages[] = 'Số điện thoại phải là số';
                }
            }

        }
        // Check address
        $address = isset($request['address']) ? $request['address'] : '';
        if ($address == '' || mb_strlen($address) > 250) {
            if ($address == '') {
                $messages[] = 'Địa chỉ là bắt buộc.';
            } else {
                $messages[] = 'Địa chỉ không được lớn hơn 250 ký tự.';
            }
        }
        // check cancellation_reason
        $order_status_id = $request['order_status_id'];
        $cancellation_reason = isset($request['cancellation_reason']) ? $request['cancellation_reason'] : '';
        if ($order_status_id == 6 && $cancellation_reason == '') {
            $messages[] = 'Lí do hủy không được để trống';
        }

        return $messages;
    }

    private function data_before()
    {
        $products = DB::table('products')->select('id', 'name')->pluck('name', 'id')->toArray();
        $orders_status = DB::table('orders_status')->select('id', 'status')->get();
        // lấy tên status
        $orders_status_name = DB::table('orders_status')->select('id', 'status')->pluck('status', 'id')->toArray();
        return [
            'products' => $products,
            'orders_status' => $orders_status,
            'orders_status_name' => $orders_status_name,
        ];
    }
}
