<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;
use App\Models\news;
use App\Models\products;
use App\Models\slide_image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

class NewsController extends AdminController
{
    public function index() {

        $news = DB::table('news')->select('id', 'title', 'link_news', 'link_image', 'content', 'publish', 'created_at', 'update_at')->where('active', 1);
        $request = request()->all();
        // tìm kiếm
        $title = isset($request['title']) ? $request['title'] : '';
        if ($title != '') {
            $news = $news->where('title', 'like', '%' . $title . '%');
        }
        $publish = isset($request['publish']) ? $request['publish'] : '';
        if ($publish != '') {
            $news = $news->where('publish', $publish);
        }
        // Price_from
        $price_from = isset($request['price_from']) ? $request['price_from'] : '';
        if ($price_from != '') {
            $news = $news->where('price', '>=', $price_from);
        }
        // Price_to
        $price_to = isset($request['price_to']) ? $request['price_to'] : '';
        if ($price_to != '') {
            $news = $news->where('price', '<=', $price_to);
        }
        $news = $news->orderBy('id', 'desc')->paginate(10);
        $obj['news'] = $news;
        // thống kê
        $total_news = DB::table('news')->select( DB::raw('count(news.id) as total_news'))->where('active', 1)->first();
        $news_not_displayed = DB::table('news')->select( DB::raw('count(news.id) as news_not_displayed'))->where('active', 1)->where('publish', 0)->first();
        $obj['total_news'] = $total_news;
        $obj['news_not_displayed'] = $news_not_displayed;
        $this->php();
        return view('admin_news.news')->with($obj);
    }
    public function create() {
        return view('admin_news.news_form');
    }
    public function store() {
        session()->flashInput(request()->input());
        $request = request()->all();
        $messages = $this->validate_news();
        $image = isset($request['photos']) ? $request['photos'] : '';
        if ($image == '') {
            $messages[] = 'Hình ảnh không được để trống';
        }
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            return view('admin_news.news_form');
        } else {
            // Insert vào cùng bảng
            $request['link_image'] = $this->news_image();
            (new \App\Models\news)->create($request);
            Session::flash('message', 'Tạo sản phẩm thành công!');
            return redirect('/news');
        }
    }
    public function edit($id) {

        $news = DB::table('news')->select('id', 'title', 'link_news', 'link_image', 'content', 'publish')->where('id', $id)->where('active', 1)->first();
        if (!$news) {
            abort(404);
        }
        $obj['news'] = $news;
        return view('admin_news.news_form')->with($obj);
    }
    public function update() {
        session()->flashInput(request()->input());
        $messages = $this->validate_news();
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            $obj['news'] = (object)(request()->all());
            return view('admin_news.news_form')->with($obj);
        } else {
            $request = request()->all();
            if (isset($request['photos'])) {
                $request['link_image'] = $this->news_image();
            }
            news::find($request['id'])->update($request);
            Session::flash('message', 'Cập nhật sản phẩm thành công!');
            return redirect('/news');
        }
    }
    public function destroy($id) {
        news::find($id)->update(['active' => 0]);
        Session::flash('message', 'Xóa sản phẩm thành công');
        return redirect('/news');
    }
    private function validate_news() {
        $request = request()->all();
        $messages = [];
        //Check title
        $title = isset($request['title']) ? $request['title'] : '';
        if ($title == '' || mb_strlen($title) > 250) {
            if ($title == '') {
                $messages[] = 'Tiêu đề là bắt buộc.';
            } else {
                $messages[] = 'Tiêu đề không được lớn hơn 250 ký tự.';
            }
        }
        // Check content
        $content = isset($request['content']) ? $request['content'] : '';
        if ($content == '') {
            $messages[] = 'Nội dung không được để trống';
        }
        return $messages;
    }
    //Xử lý ảnh, lưu một ảnh
    private function news_image()
    {
        $images = request()->file('photos')->getClientOriginalName();
        $path = '';
        if ($images) {
            $path = '/images/slide/' . $images;
            $new_img = Image::make(request()->file('photos')->getRealPath())->fit(683, 519);
            // save file with medium quality
            $new_img->save(public_path() . $path, 80);
        }
        return $path;
    }
    private function php()
    {


    }
}
