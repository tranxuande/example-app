<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;
use App\Models\slide_image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;


class SlideImageController extends AdminController
{
    public function index()
    {
        $obj = $this->data_before();
        $slide_image = DB::table('slide_image')->select('id', 'name', 'link_image', 'product_id', 'product_link', 'publish', 'active', 'created_at', 'update_at')->where('active', 1);
        $request = request()->all();
        // Tìm kiếm theo từng trường
        // Name
        $name = isset($request['name']) ? $request['name'] : '';
        if ($name != '') {
            $slide_image = $slide_image->where('name', 'like', '%' . $name . '%');
        }
        // Category
        $category = isset($request['category']) ? $request['category'] : '';
        if ($category != '') {
            $slide_image = $slide_image->where('product_id', $category);
        }
        // Publish
        $publish = isset($request['publish']) ? $request['publish'] : '';
        if ($publish != '') {
            $slide_image = $slide_image->where('publish', $publish);
        }
        // Date_from
        $date_from = isset($request['date_from']) ? $request['date_from'] : '';
        if ($date_from != '') {
            $slide_image = $slide_image->where('created_at', '>=', $date_from);
        }
        // Date_to
        $date_to = isset($request['date_to']) ? $request['date_to'] . ' 23:59:59' : '';
        if ($date_to != '') {
            $slide_image = $slide_image->where('created_at', '<=', $date_to);
        }
        $slide_image = $slide_image->orderBy('id', 'desc')->paginate(10);
        $obj['slide_image'] = $slide_image;
        // thống kê
        // thống kê
        $total_slide = DB::table('slide_image')->select( DB::raw('count(slide_image.id) as total_slide'))->where('active', 1)->first();
        $slide_not_displayed = DB::table('slide_image')->select( DB::raw('count(slide_image.id) as slide_not_displayed'))->where('active', 1)->where('publish', 0)->first();
        $obj['total_slide'] = $total_slide;
        $obj['slide_not_displayed'] = $slide_not_displayed;
        return view('admin_slide_image.slide_image')->with($obj);
    }

    public function create()
    {
        $obj = $this->data_before();
        return view('admin_slide_image.slide_image_form')->with($obj);
    }

    public function store()
    {
        $obj = $this->data_before();
        session()->flashInput(request()->input());
        $request = request()->all();
        $messages = $this->validate_slide();
        $image = isset($request['photos']) ? $request['photos'] : '';
        if ($image == '') {
            $messages[] = 'Hình ảnh không được để trống';
        }
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            return view('admin_slide_image.slide_image_form')->with($obj);
        } else {
            // Insert vào cùng bảng
            $request['link_image'] = $this->slide_image();
            (new \App\Models\slide_image)->create($request);
            Session::flash('message', 'Tạo sản phẩm thành công!');
            return redirect('/slide');
        }
    }

    public function edit($id)
    {
        $obj = $this->data_before();
        $slide_image = DB::table('slide_image')->select('id', 'name', 'product_id', 'publish', 'link_image')->where('id', $id)->where('active', 1)->first();
        if (!$slide_image) {
            abort(404);
        }
        $obj['slide_image'] = $slide_image;
        return view('admin_slide_image.slide_image_form')->with($obj);
    }

    public function update()
    {
        $obj = $this->data_before();
        session()->flashInput(request()->input());
        $messages = $this->validate_slide();
        if ($messages) {
            Session::flash('message', join('<br>', $messages));
            $obj['slide_image'] = (object)(request()->all());
            return view('admin_slide_image.slide_image_form')->with($obj);
        } else {
            $request = request()->all();
            if (isset($request['photos'])) {
                $request['link_image'] = $this->slide_image();
            }
            slide_image::find($request['id'])->update($request);
            Session::flash('message', 'Cập nhật sản phẩm thành công!');
            return redirect('/slide');
        }
    }

    public function destroy($id)
    {
        slide_image::find($id)->update(['active' => 0]);
        Session::flash('message', 'Xóa sản phẩm thành công');
        return redirect('/slide');
    }

    //Xử lý ảnh, lưu một ảnh
    private function slide_image()
    {
        $images = request()->file('photos')->getClientOriginalName();
        $path = '';
        if ($images) {
            $path = '/images/slide/' . $images;
            $new_img = Image::make(request()->file('photos')->getRealPath())->fit(1920, 700);
            // save file with medium quality
            $new_img->save(public_path() . $path, 80);
        }
        return $path;
    }

    private function validate_slide()
    {
        $request = request()->all();
        $messages = [];
        //Check name
        $name = isset($request['name']) ? $request['name'] : '';
        if ($name == '' || mb_strlen($name) > 250) {
            if ($name == '') {
                $messages[] = 'Trường tên là bắt buộc.';
            } else {
                $messages[] = 'Trường tên không được lớn hơn 250 ký tự.';
            }
        }
        //Check product
        $product = isset($request['product_id']) ? $request['product_id'] : '';
        if ($product == '' || !is_int((int)$product)) {
            if ($product == '') {
                $messages[] = 'Sản phẩm không được để trống.';
            } else {
                $messages[] = 'Sản phẩm định dạng dữ liệu bị sai.';
            }
        }
        return $messages;
    }

    private function data_before()
    {
        $products = DB::table('products')->select('id', 'name')->pluck('name', 'id')->toArray();
        return [
            'products' => $products
        ];
    }
}
