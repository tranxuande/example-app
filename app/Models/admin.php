<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;

class admin extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    protected $table = 'admin';
    protected $primaryKey = 'id';
    protected $fillable = ['username', 'password'];

    public $timestamps = TRUE;

    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    public function getAuth($u, $p)
    {
        $p = md5($p);
        return $this->where([
            ['username', $u],
            ['password', $p],
            ['active', 1],
        ])->first();
    }
}
