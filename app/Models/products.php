<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * @mixin Builder
 */
class products extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'category_id', 'price', 'promotional_price', 'percent_discount', 'description', 'content', 'publish', 'active','quantily', 'sold'];

    public $timestamps = TRUE;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'update_at';

    public static function boot()
    {
        parent::boot();
        self::saving(function ($model) {
            // phan tram giam gia
            if ($model->promotional_price == '') {
                $model->percent_discount = null;
            } else {
                $percent = (($model->price - $model->promotional_price) / $model->price) * 100;
                $model->percent_discount =round($percent) ;
            }

            // link san pham
            $name = $model->name;
            $link = convert_vi_to_en($name);
            $link = strtolower($link);
            $link = preg_replace('/[^A-Za-z0-9\s]/', '', $link);
            $link = preg_replace('/\s+/', '-', $link);
            if (!!$model->id) {
                $link .= '-' . $model->id;
            } else {
                $count = 0;
                $id_db = DB::table('products')->select('id')->orderBy('id', 'desc')->first();
                if ($id_db) {
                    $count = $id_db->id;
                }
                $link .= '-' . ($count + 1);
            }
            $model->link = $link;
        });
    }

    function dataAdminIndex()
    {
        return $this->all('id', 'name', 'category_id', 'price', 'promotional_price', 'publish', 'created_at', 'update_at');
    }
}
