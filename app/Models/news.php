<?php


namespace App\Models;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class news extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;
    protected $table = 'news';
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'link_news', 'link_image', 'content', 'publish', 'active'];
    public $timestamps = TRUE;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'update_at';
    public static function boot()
    {
        parent::boot();
        self::saving(function ($model) {

            // link san pham
            $title = $model->title;
            $link = convert_vi_to_en($title);
            $link = strtolower($link);
            $link = preg_replace('/[^A-Za-z0-9\s]/', '', $link);
            $link = preg_replace('/\s+/', '-', $link);
            if (!!$model->id) {
                $link .= '-' . $model->id;
            } else {
                $count = 0;
                $id_db = DB::table('news')->select('id')->orderBy('id', 'desc')->first();
                if ($id_db) {
                    $count = $id_db->id;
                }
                $link .= '-' . ($count + 1);
            }
            $model->link_news = $link;
        });

    }
}
