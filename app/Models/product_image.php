<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;

/**
 * @mixin Builder
 */
class product_image extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    protected $table = 'product_image';
    protected $primaryKey = 'id';
    protected $fillable = ['product_id', 'link', 'active'];

    public $timestamps = FALSE;
}
