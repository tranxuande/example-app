<?php


namespace App\Models;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class orders_details extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;
    protected $table = 'orders_details';
    protected $primaryKey = 'id';
    protected $fillable = ['orders_id', 'product_id', 'money', 'quantity'];
    public $timestamps = FALSE;

    public static function boot() {
        parent::boot();
        self::saving(function ($model) {
            if (!$model->id){
                dd(111);
            }



        });
    }
}
