<?php


namespace App\Models;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class pay extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;
    protected $table = 'orders';
    protected $primaryKey = 'id';
    protected $fillable = ['code_orders', 'full_name', 'phone_number', 'address', 'order_status_id'];
    public $timestamps = TRUE;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'update_at';

    public static function boot()
    {
        parent::boot();
        self::saving(function ($model) {
            // trạng thái
            if (!$model->id) {
                $model->order_status_id = 1;
                $model->code_orders = 'MDH' . '-' . date('Ymd') . '-' . '001';
            }
            if (!!$model->id) {
                $model->code_orders = 'MDH' . '-' . date('Ymd') . '-' . '002';
            }

        });
    }
}
