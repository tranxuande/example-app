<?php


namespace App\Models;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @mixin Builder
 */
class slide_image extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    protected $table = 'slide_image';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'product_id', 'link_image', 'publish', 'active'];

    public $timestamps = TRUE;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'update_at';

    public static function boot()
    {
        parent::boot();
        self::saving(function ($model) {
            // link san pham
            if (!!$model->product_id) {
                $products = DB::table('products')->select('link')->where('id', $model->product_id)->first();
                $model->product_link = $products->link;
            } else {
                $model->product_link = null;
            }
        });
    }


}
