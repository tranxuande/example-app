<?php


namespace App\Models;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class orders extends Model implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable;
    protected $table = 'orders';
    protected $primaryKey = 'id';
    protected $fillable = ['full_name', 'phone_number', 'address', 'note', 'order_status_id', 'cancellation_reason', 'active'];
    public $timestamps = TRUE;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'update_at';

    public static function boot()
    {
        parent::boot();
        self::saving(function ($model) {
            // trạng thái
            $id_db = DB::table('orders')->select('id')->orderBy('id', 'desc')->first();
            if ($id_db) {
                $count = $id_db->id;
                if ( !$model->id) {
                    $num = $count + 1 ;
                    $num_padded = sprintf("%03d", $num);
                    $model->order_status_id = 1;
                    $model->code_orders = 'MDH' . '-' . date('YmdHis') . '-' . $num_padded;
                }

            } else {
                if (!$model->id) {
                    $num = 1;
                    $num_padded = sprintf("%03d", $num);
                    $model->order_status_id = 1;
                    $model->code_orders = 'MDH' . '-' . date('YmdHis') . '-' . $num_padded;
                }
            }

        });
    }

}
