<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOrdersStatusTable extends Migration
{

    public function up()
    {
        Schema::create('orders_status', function (Blueprint $table) {
            $table->id();
            $table->text('status');
        });

        //Insert
        DB::table('orders_status')->insert([
            ['status' => 'Chờ'],
            ['status' => 'Duyệt'],
            ['status' => 'Đang giao hàng'],
            ['status' => 'Đã giao hàng'],
            ['status' => 'Hoàn thành'],
            ['status' => 'Hủy'],
        ]);
    }


    public function down()
    {
        Schema::dropIfExists('orders_status');
    }
}
