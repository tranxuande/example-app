<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('category')) {
            Schema::create('category', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50);
                $table->integer('parent_id')->nullable();
                $table->string('link_category', 500);
            });

            // Insert
            DB::table('category')->insert([
                [
                    'name' => 'Thời trang nữ',
                    'parent_id' => null,
                    'link_category' => 'thoi-trang-nữ',
                ],
                [
                    'name' => 'Thời trang nam',
                    'parent_id' => null,
                    'link_category' => 'thoi-trang-nam',
                ],
                [
                    'name' => 'Thời trang trẻ em',
                    'parent_id' => null,
                    'link_category' => 'thoi-trang-tre-em',
                ],
                [
                    'name' => 'Quần nữ',
                    'parent_id' => 1,
                    'link_category' => 'quần-nữ',
                ],
                [
                    'name' => 'Áo nữ',
                    'parent_id' => 1,
                    'link_category' => 'áo-nữ',
                ],
                [
                    'name' => 'Quần nam',
                    'parent_id' => 2,
                    'link_category' => 'quần-nam',
                ],
                [
                    'name' => 'Áo nam',
                    'parent_id' => 2,
                    'link_category' => 'áo-nam',
                ],
                [
                    'name' => 'Quần trẻ em',
                    'parent_id' => 3,
                    'link_category' => 'quần-trẻ-em',
                ],
                [
                    'name' => 'Áo trẻ em',
                    'parent_id' => 3,
                    'link_category' => 'áo-trẻ-em',
                ],
            ]);
        }
    }


    public function down()
    {
        Schema::dropIfExists('category');
    }
}
