<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('admin')) {
            Schema::create('admin', function (Blueprint $table) {
                $table->id();
                $table->string('username', 100);
                $table->string('password', 100);
                $table->boolean('active')->default(0);
                $table->dateTime('created_at');
                $table->dateTime('update_at');
            });

        }

    }

    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
