<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('code_orders', 100);
            $table->text('full_name');
            $table->string('phone_number', 10);
            $table->string('address', 250);
            $table->text('note')->nullable();
            $table->integer('order_status_id');
            $table->string('cancellation_reason', 250)->nullable();
            $table->boolean('active')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('update_at');
        });
    }
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
