<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{

    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title', 250);
            $table->string('link_news', 500);
            $table->string('link_image', 500);
            $table->text('content');
            $table->boolean('publish')->default(0);
            $table->boolean('active')->default(1);
            $table->dateTime('created_at');
            $table->dateTime('update_at');
        });
    }


    public function down()
    {
        Schema::dropIfExists('news');
    }
}
