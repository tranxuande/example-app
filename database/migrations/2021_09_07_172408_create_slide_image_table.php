<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideImageTable extends Migration
{
    public function up()
    {
        Schema::create('slide_image', function (Blueprint $table) {
            $table->id();
            $table->string('name', 250);
            $table->string('link_image', 500);
            $table->integer('product_id');
            $table->string('product_link');
            $table->boolean('active')->default(1);
            $table->boolean('publish')->default(0);
            $table->dateTime('created_at');
            $table->dateTime('update_at');
        });
    }
    public function down()
    {
        Schema::dropIfExists('slide_image');
    }
}
