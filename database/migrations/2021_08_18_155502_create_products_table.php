<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('products')) {
            Schema::create('products', function (Blueprint $table) {
                $table->id();
                $table->string('name', 250);
                $table->integer('category_id');
                $table->decimal('price', 12, 0);
                $table->decimal('promotional_price', 12, 0)->nullable();
                $table->string('description', 500);
                $table->text('content');
                $table->float('percent_discount')->nullable();
                $table->string('link', 500);
                $table->integer('quantily');
                $table->integer('sold')->nullable();
                $table->boolean('publish')->default(0);
                $table->boolean('active')->default(1);
                $table->dateTime('created_at');
                $table->dateTime('update_at');
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
