<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersDetailsTable extends Migration
{

    public function up()
    {
        Schema::create('orders_details', function (Blueprint $table) {
            $table->id();
            $table->integer('orders_id');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->decimal('money', 12, 0);
            $table->decimal('total_money', 12, 0);
            $table->boolean('active')->default(1);
        });
    }


    public function down()
    {
        Schema::dropIfExists('orders_details');
    }
}
